

/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


controllers.reportSpiderLog = function($scope, $rootScope, $routeParams, $timeout, Reports, Spiders, Run){
    
    $timeout(function(){
        $rootScope.$broadcast('setCurrent','report');
        $rootScope.$broadcast('setCurrentC','spiderLog');
    });
    
    $scope.limit = 100;
    
    var timer = null;
    var timer2 = null;
    
    $scope.hideContext = function(){
        $("#context").hide();
    };
    
    $scope.showContext = function(log){
        console.debug(log);
        $scope.currentContext = log.context;
        $("#context").show();
    };
    
    $scope.launchSpider = function(spider){
        Run.launchSpider(spider.id).success(function(){
            Spiders.getAll().then(function(s){
                for(var i in s){
                    $scope.spiders[i].runningStatus = s[i].runningStatus;
                }
            });
        });
    };
    
    $scope.stopSpider = function(spider){
        Run.stopSpider(spider.id).success(function(){
            Spiders.getAll().then(function(s){
                for(var i in s){
                    $scope.spiders[i].runningStatus = s[i].runningStatus;
                }
            });
        });
    };
    
    $scope.enable = function(i){
        $scope.spiders[i].enable=true;
    };
    
    $scope.disable = function(i){
        $scope.spiders[i].enable=true;
    };
    
    $scope.filterLogType = function(logType){
        logType.selected = !logType.selected;
        
        var logTypes = [];
        for(var i in $scope.logTypes){
            if($scope.logTypes[i].selected === true){
                logTypes.push($scope.logTypes[i].id);
            }
        }
        
        var spiders = [];
        for(var i in $scope.spiders){
            if($scope.spiders[i].enable === true){
                spiders.push($scope.spiders[i].id);
            }
        }
        
        Reports.getAllSpiderLogs($scope.limit, logTypes, spiders).success(function(l){
            $scope.spiderLogs = l;
        });
        
    };
    
    $scope.filterSpider = function(spider){
        spider.enable = !spider.enable;
        
        var logTypes = [];
        for(var i in $scope.logTypes){
            if($scope.logTypes[i].selected === true){
                logTypes.push($scope.logTypes[i].id);
            }
        }
        
        var spiders = [];
        for(var i in $scope.spiders){
            if($scope.spiders[i].enable === true){
                spiders.push($scope.spiders[i].id);
            }
        }
        
        
        Reports.getAllSpiderLogs($scope.limit, logTypes, spiders).success(function(l){
            $scope.spiderLogs = l;
        });
        
    };
    
    
    
    
    
    Spiders.getAll().then(function(s){
        $scope.spiders = s;
        for(var i in $scope.spiders){
            if(typeof $routeParams.spiderID == 'undefined'){
                $scope.spiders[i].enable = true;
            }
            else if(typeof $routeParams.spiderID != 'undefined' && $routeParams.spiderID==$scope.spiders[i].id){
                $scope.spiders[i].enable = true;
            }
            else{
                $scope.spiders[i].enable = false;
            }
            
        }
        console.debug($scope.spiders);
        
        timer2 = setInterval(function(){
            Spiders.getAll().then(function(s){
                for(var i in s){
                    $scope.spiders[i].runningStatus = s[i].runningStatus;
                }
            });
        }, 30000);
        
        
        Reports.getLogTypes().success(function(l){
            
            for(var i in l){
                l[i].selected = true;
            }
            $scope.logTypes = l;
            
            
            var logTypes = [];
            for(var i in $scope.logTypes){
                if($scope.logTypes[i].selected === true){
                    logTypes.push($scope.logTypes[i].id);
                }
            }

            var spiders = [];
            for(var i in $scope.spiders){
                if($scope.spiders[i].enable === true){
                    spiders.push($scope.spiders[i].id);
                }
            }
        
            Reports.getAllSpiderLogs($scope.limit, logTypes, spiders).success(function(l){
                
                $scope.spiderLogs = l;

                timer = setInterval(function(){
                    var logTypes = [];
                    for(var i in $scope.logTypes){
                        if($scope.logTypes[i].selected === true){
                            logTypes.push($scope.logTypes[i].id);
                        }
                    }
                    
                    var spiders = [];
                    for(var i in $scope.spiders){
                        if($scope.spiders[i].enable === true){
                            spiders.push($scope.spiders[i].id);
                        }
                    }

                    Reports.getSpiderLogsSince( logTypes , spiders).success(function(l){
                        for(var i in l){
                            $scope.spiderLogs.unshift(l[i]);
                            $scope.spiderLogs.pop();
                        }
                    });

                }, 2000);
                
            });
        });
            
            
        });
        
        
    
    $scope.$on('$destroy', function(){
        clearInterval(timer);
        clearInterval(timer2);
    });
    
};

