/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


controllers.testGoogleChart = function($scope, $rootScope, $modal, $timeout, $routeParams, Spiders) {

var d1 = new Date();
d1.setDate(5);
d1.setMonth(0);

var d2 = new Date();
d1.setDate(6);
d1.setMonth(0);

var d3 = new Date();
d1.setDate(7);
d1.setMonth(0);

var d4 = new Date();
d1.setDate(8);
d1.setMonth(0);

var d5 = new Date();
d1.setDate(9);
d1.setMonth(0);

    $scope.tmpStatChart = {
        type: "LineChart",
        "displayed": true,
        "cssStyle": "height:600px; width:1400px;",
        "data": {
            "cols": [
                {
                    label: "Day",
                    type: "date",
                    "p": {}
                },
                {
                    label: "Live Jobs",
                    type: "number",
                    "p": {}
                },
                {
                    label: "Spidered Job URLs",
                    type: "number",
                    "p": {}
                },
                {
                    label: "Added Jobs",
                    type: "number",
                    "p": {}
                },
                {
                    label: "Deleted Jobs",
                    type: "number",
                    "p": {}
                }
            ],
            'rows': [
                {
                    c: [
                        {
                            v:  new Date(2013,1,1)
                        },
                        {
                            v: 10,
                            f : "10"
                        },
                        {
                            v: 20
                        },
                        {
                            v: 30
                        },
                        {
                            v: 40
                        }
                    ]
                },
                {
                    c: [
                        {
                           v:  new Date(2013,1,2)
                        },
                        {
                            v: 20,
                            f : "20"
                        },
                        {
                            v: 30
                        },
                        {
                            v: 40
                        },
                        {
                            v: 50
                        }
                    ]
                },
                {
                    c: [
                        {
                            v:  new Date(2013,1,3)
                        },
                        {
                            v: 30,
                            f : "30"
                        },
                        {
                            v: 40
                        },
                        {
                            v: 50
                        },
                        {
                            v: 60
                        }
                    ]
                },
                {
                    c: [
                        {
                            v:  new Date(2013,1,4)
                        },
                        {
                            v: 40,
                            f : "40"
                        },
                        {
                            v: 50
                        },
                        {
                            v: 60
                        },
                        {
                            v: 70
                        }
                    ]
                }


            ]

        },
        "options": {
            "title": "Daily Statistics",
            "isStacked": "false",
            "fill": 20,
            "displayExactValues": true,
            "height": 600,
            "width": 1400,
            "vAxis": {
                "title": "Nb Jobs",
                "gridlines": {
                    "count": 10
                }
            },
            "hAxis": {
                "title": "Date"
            },
            "legend": {
                "alignment": 'top'
            }
        },
        "formatters": {}
    };

    console.debug($scope.tmpStatChart);
};