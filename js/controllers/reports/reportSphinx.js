/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */


controllers.reportSphinx = function($scope, $rootScope, $routeParams,Sphinx){
    $rootScope.$broadcast('setCurrent','report');
    $rootScope.$broadcast('setCurrentC','sphinx');
    $scope.testAdmin = $rootScope.testAdmin;

    $scope.processing = false;

    Sphinx.statusAll().then(function(status){
        $scope.status=status;
        console.debug(status);
    });

};