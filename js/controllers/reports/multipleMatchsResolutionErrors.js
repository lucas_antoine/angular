/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */

controllers.multipleMatchsResolutionErrors = function($scope, $rootScope, $timeout, Reports) {


    $timeout(function() {
        $rootScope.$broadcast('setCurrent', 'report');
        $rootScope.$broadcast('setCurrentC', 'resolutionErrors');
    });
    
    
};

