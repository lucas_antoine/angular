'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.reportLogs', []);

//alias of seoPageData
//app.controller('reportLogs',['$scope', '$rootScope', '$routeParams'
//        , function($scope, $rootScope, $routeParams){
controllers.reportLogs = function($scope, $rootScope, $routeParams){
    $rootScope.$broadcast('setCurrent','report');
    $rootScope.$broadcast('setCurrentC','logs');
    $scope.testAdmin = $rootScope.testAdmin;

    $scope.processing = false;

};


