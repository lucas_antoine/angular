'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.reportSpider', []);

//alias of seoPageData
//app.controller('reportSpiders',['$scope', '$rootScope', '$routeParams', 'Spiders'
//        , function($scope, $rootScope, $routeParams,Spiders){
controllers.reportSpiders = function($scope, $rootScope, $modal, $timeout, $routeParams, Spiders) {
    $rootScope.$broadcast('setCurrent', 'report');
    $rootScope.$broadcast('setCurrentC', 'spider');
    $scope.testAdmin = $rootScope.testAdmin;

    $scope.processing = false;




    Spiders.getGeneralStats().then(function(stats) {
        $scope.generalStats = stats;
        
        

        console.debug($scope.generalStats);

        for (var j in $scope.generalStats) {
            
            
            var tmpStatChart= {
                "type": "LineChart",
                "displayed": true,
                "cssStyle": "height:600px; width:1400px;",
                "data": {
                    "cols": [
                        {
                            "label": "Day",
                            "type": "date",
                            "p": {}
                        },
                        {
                            "label": "Live Jobs",
                            "type": "number",
                            "p": {}
                        },
                        {
                            "label": "Spidered Job URLs",
                            "type": "number",
                            "p": {}
                        },
                        {
                            "label": "Added Jobs",
                            "type": "number",
                            "p": {}
                        },
                        {
                            "label": "Deleted Jobs",
                            "type": "number"
                        }
                    ],
                    'rows': []

                },
                "options": {
                    "title": "Daily Statistics",
                    "isStacked": "false",
                    "fill": 20,
                    "displayExactValues": true,
                    "height" : 600,
                    "width" : 1400,
                    "vAxis": {
                        "title": "Nb Jobs",
                        "gridlines": {
                            "count": 10
                        }
                    },
                    "hAxis": {
                        "title": "Date"
                    },
                    "legend" : {
                        "alignment" : 'top'
                    }
                },
                "formatters": {}
            };
            var tmpTimeChart = {
                "type": "LineChart",
                "displayed": true,
                "cssStyle": "height:600px; width:1400px;",
                "data": {
                    "cols": [
                        {
                            "label": "Date",
                            "type": "date",
                            "p": {}
                        },
                        {
                            "label": "Removing Unspidered Jobs",
                            "type": "number",
                            "p": {}
                        },
                        {
                            "label": "Removing Old Jobs",
                            "type": "number",
                            "p": {}
                        },
                        {
                            "label": "Spidering Search URLs",
                            "type": "number",
                            "p": {}
                        },
                        {
                            "label": "Spidering 404 URLs",
                            "type": "number"
                        },
                        {
                            "label": "Spidering Job URLs",
                            "type": "number"
                        },
                        {
                            "label": "Processing Jobs",
                            "type": "number"
                        },
                        {
                            "label": "Resolving Classification",
                            "type": "number"
                        },
                        {
                            "label": "Recording Stats",
                            "type": "number"
                        }
                    ],
                    'rows': []

                },
                "options": {
                    "title": "Execution time",
                    "isStacked": "false",
                    "fill": 20,
                    "displayExactValues": true,
                    "height" : 600,
                    "width" : 1400,
                    "vAxis": {
                        "title": "Time in Second",
                        "gridlines": {
                            "count": 10
                        }
                    },
                    "hAxis": {
                        "title": "Date",
                        "gridlines": {
                            "count": 10
                        },
                        "format" : 'MMM d',
                        showTextEvery:1/*,
                        minorGridlines : {
                            color : '#777',
                            count : 3
                        }*/
                    },
                    "legend" : {
                        "alignment" : 'top'
                    }
                    
                },
                "formatters": {}
            };

            for (var i in $scope.generalStats[j].stats) {

                var c = [];
                 
                c.push({
                    "v": new Date($scope.generalStats[j].stats[i].date[0], $scope.generalStats[j].stats[i].date[1], $scope.generalStats[j].stats[i].date[2])
                });
                c.push({
                    "v": $scope.generalStats[j].stats[i].nbLiveJobs
                });
                c.push({
                    "v": $scope.generalStats[j].stats[i].nbJobsSpideredToday
                });
                c.push({
                    "v": $scope.generalStats[j].stats[i].nbJobsAddedToday
                });
                c.push({
                    "v": $scope.generalStats[j].stats[i].nbJobsDeletedToday
                });

                tmpStatChart.data.rows.push({
                    "c": c
                });

            }
            
            $scope.generalStats[j].statChart = tmpStatChart;

            console.debug($scope.generalStats[j].statChart);
            
            for (var i in $scope.generalStats[j].time) {

                var c = [];
                
                        
                //searchURLID : '22845' over jobURLID : '412778'	
                c.push({
                    v: new Date($scope.generalStats[j].time[i].date[0], $scope.generalStats[j].time[i].date[1], $scope.generalStats[j].time[i].date[2])
                });
                c.push({
                    v : $scope.generalStats[j].time[i].data['Removing Unspidered Jobs']['seconds'],
                    f : $scope.generalStats[j].time[i].data['Removing Unspidered Jobs']['nbInstances']+" instances" 
                });
                c.push({
                    v : $scope.generalStats[j].time[i].data['Removing Old Jobs']['seconds'],
                    f : $scope.generalStats[j].time[i].data['Removing Old Jobs']['nbInstances'] +" instances" 
                });
                c.push({
                    v : $scope.generalStats[j].time[i].data['Spidering Search URLs']['seconds'],
                    f : $scope.generalStats[j].time[i].data['Spidering Search URLs']['nbInstances'] +" instances" 
                });
                c.push({
                    v : $scope.generalStats[j].time[i].data['Spidering for 404 URLs']['seconds'],
                    f : $scope.generalStats[j].time[i].data['Spidering for 404 URLs']['nbInstances'] +" instances" 
                });
                c.push({
                    v : $scope.generalStats[j].time[i].data['Spidering Job URLs']['seconds'],
                    f : $scope.generalStats[j].time[i].data['Spidering Job URLs']['nbInstances'] +" instances" 
                });
                c.push({
                    v : $scope.generalStats[j].time[i].data['Processing Jobs']['seconds'],
                    f : $scope.generalStats[j].time[i].data['Processing Jobs']['nbInstances'] +" instances" 
                });
                c.push({
                    v : $scope.generalStats[j].time[i].data['Resolving Classifications']['seconds'],
                    f : $scope.generalStats[j].time[i].data['Resolving Classifications']['nbInstances'] +" instances" 
                });
                c.push({
                    v : $scope.generalStats[j].time[i].data['Recording Stats']['seconds'],
                    f : $scope.generalStats[j].time[i].data['Recording Stats']['nbInstances'] 
                });

                tmpTimeChart.data.rows.push({
                    "c": c
                });

            }
            
            
            $scope.generalStats[j].timeChart = tmpTimeChart;
            console.debug($scope.generalStats[j].timeChart);
        }





    });

    $scope.loadSearchErrors = function(spiderID, errorID) {
        Spiders.getSearchErrors(spiderID, errorID).then(function(e) {
            console.debug(e);
            $scope.searchURLErrors = e;
            var modalInstance = $modal.open({
                templateUrl: '/partials/reports/modalURL.html',
                controller: function($scope, $modalInstance, urls) {

                    $scope.urls = urls;


                    $scope.ok = function( ) {
                        $modalInstance.close();
                    };

                    $scope.cancel = function( ) {
                        $modalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    urls: function() {
                        return $scope.searchURLErrors;
                    }
                }
            });

            modalInstance.result.then(function() {
                //$scope.selected = selectedItem;
            }, function() {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        });
    };

};


