'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.CspidersEdit', []);

//app.controller('spiderEdit', ['$scope', '$rootScope', '$routeParams', 'Spiders',  'Origins', 'Branches', 'Company'
//            , function($scope, $rootScope, $routeParams, Spiders,   Origins, Branches, Company) {
controllers.spiderEdit = function($scope, $rootScope, $routeParams, Spiders,   Origins, Branches, Company) {


        $rootScope.$broadcast('setCurrent', 'spider');
        $rootScope.$broadcast('setCurrentW', 'definition');
        
        $rootScope.$broadcast('setCurrentSpiderID',$routeParams.spiderID);

        $scope.processing = false;
        $scope.testAdmin = $rootScope.testAdmin;
        $scope.error = false;
        
        Spiders.get($routeParams.spiderID).then(function(s){
            $scope.spider = s;
            $scope.countryName=$scope.spider.countryName;
            $scope.originName=$scope.spider.originName;            
        });
        
        $scope.save = function(){
            $scope.errorMsg = "";
            $scope.error = false;
            if($scope.origin.branchID==0){
                $scope.errorMsg += "The Branch is Missing. "; 
                $scope.error=true;
            }
             if($scope.spider.countryID==0){
                $scope.error=true;
                $scope.errorMsg += "The country is Missing."; 
            }
            if(!$scope.error){
                $scope.processing=true;
                Origins.doSave($scope.origin).success(function(){
                    Spiders.doSave($scope.spider).then(function(){
                        $scope.processing=false;
                    });
                });                
            }
        };
        /*

        
        
        

        $scope.create = function(){
            $scope.processing=true;
            $scope.origin.branchID = $scope.autocompleteBranchID;
            Origins.doCreate($scope.origin).then(function(data) {
                $scope.spider.originID = data.id;
                $scope.spider.countryID = $scope.autocompleteCountryID;
                        
                Spiders.doCreate($scope.spider).then(function(id){
                    $rootScope.$broadcast('setCurrentSpiderID', id);                    
                });
            });            
        };*/

};