'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.CspidersCreate', []);

//app.controller('spiderCreate', ['$scope', '$rootScope', '$routeParams', 'Spiders', 'Location', 'Origins'
//            , function($scope, $rootScope, $routeParams, Spiders, Location, Origins) {
controllers.spiderCreate = function($scope, $rootScope, $routeParams, Spiders, Location, Origins) {


        $rootScope.$broadcast('setCurrent', 'spider');

        $scope.processing = false;
        $scope.testAdmin = $rootScope.testAdmin;

        $scope.spider = {
            countryID : 0,
            originID : 0,
            originType : 'spider'
        };

        $scope.create = function(){
            
            
            Spiders.doCreate($scope.spider).then(function(id){
                $rootScope.$broadcast('spiderCreated', id);
            });
                     
        };

};