'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.CspidersImport', []);

//app.controller('spiderImport', ['$scope', '$rootScope', '$routeParams', 'Spiders'
//            , function($scope, $rootScope, $routeParams, Spiders) {
controllers.spiderImport = function($scope, $rootScope, $routeParams, Spiders) {
    $rootScope.$broadcast('setCurrent', 'spider');
    $rootScope.$broadcast('setCurrentW', 'imports');
    $rootScope.$broadcast('setCurrentSpiderID', $routeParams.spiderID);

    
    $scope.testAdmin = $rootScope.testAdmin;

    Spiders.get($routeParams.spiderID).then(function(s) {
        $scope.spider = s;
    });

    Spiders.getImportedTypes($routeParams.spiderID).then(function(i) {
        $scope.importedTypes = i;
    });

    $scope.details = [];
    Spiders.getAvailableYamlFiles($routeParams.spiderID).then(function(i) {
        $scope.availableFiles = i;
        for (var j in i) {
            $scope.details[j] = false;
        }
    });

    $scope.remove = function(data) {
        Spiders.removeImportedClassifications($routeParams.spiderID, data.type.id).success(function() {
            Spiders.getImportedTypes($routeParams.spiderID).then(function(i) {
                $scope.importedTypes = i;
            });
        });
        console.debug(data);
    }

    $scope.process = function(i) {
        Spiders.processYAML($routeParams.spiderID, i).then(function(logs) {
            $scope.logs = logs;
        });
    }


    $scope.show = function(i) {
        $scope.details[i] = true;
    }
    $scope.hide = function(i) {
        $scope.details[i] = false;
    }
    $scope.status = function(i) {
        return $scope.details[i];
    }


};