/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


controllers.spiderResults = function($scope, $rootScope, $routeParams, Spiders, Classifications) {
    $rootScope.$broadcast('setCurrent', 'spider');
    $rootScope.$broadcast('setCurrentW', 'spiderResults');
    $rootScope.$broadcast('setCurrentSpiderID', $routeParams.spiderID);
    $scope.spiderID = $routeParams.spiderID;

    $scope.currentLocation = {
        id: 0
    };
    $scope.currentIndustry = {
        id: 0
    };
    $scope.currentStatus = {
        id: 0
    };
    $scope.results = [];
    $scope.foundRows = 0;
    
    $scope.jobURLID=0;
    $scope.jobID=0;

    function loadResults() {
        if ($scope.currentLocation.id == 0 || $scope.currentIndustry.id == 0 || $scope.currentStatus.id == 0) {
            return;
        }

        Spiders.getSpideredJobs($scope.spiderID, $scope.currentLocation.id, $scope.currentIndustry.id, $scope.currentStatus.id, 20).then(function(r) {            
            $scope.results = r.results;
            $scope.foundRows = r.foundRows;
        });

    }

    Spiders.getJobURLStatusTypes().then(function(t) {
        $scope.status = t;
        Classifications.getAllForSpider($scope.spiderID, 1, 0).then(function(r) {
            $scope.locations = r;
            Classifications.getAllForSpider($scope.spiderID, 2, 0).then(function(i) {

                $scope.industries = i;
            });
        });
    });

    $scope.loadJobURLID = function(){
        if($scope.jobURLID!=0){
            Spiders.getJobByJobURLID($scope.jobURLID).then(function(r){
                $scope.results = r.results;
                $scope.foundRows = r.foundRows;
            });
        }
    };
    
    $scope.loadJobID = function(){
        if($scope.jobID!=0){
            Spiders.getJobByJobID($scope.jobID).then(function(r){
                $scope.results = r.results;
                $scope.foundRows = r.foundRows;
            });
        }
    };
    

    $scope.setLocation = function(l) {
        $scope.currentLocation = l;
        loadResults();
    };
    $scope.setIndustry = function(i) {
        $scope.currentIndustry = i;
        loadResults();
    };
    $scope.setStatus = function(s) {
        $scope.currentStatus = s;
        loadResults();
    };



    //get classifications 


};