'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.CspidersRegexDefinition', []);

//app.controller('regexDefinition', ['$scope', '$rootScope', '$routeParams','$location', 'Spiders'
//            , function($scope, $rootScope, $routeParams,$location, Spiders) {
controllers.regexDefinition = function($scope, $rootScope, $routeParams,$location, Spiders) {
        $rootScope.$broadcast('setCurrent', 'spider');
        $rootScope.$broadcast('setCurrentW', 'regexDefinition');
        $rootScope.$broadcast('setCurrentSpiderID',$routeParams.spiderID);

        $scope.processing = false;

        $scope.testAdmin = $rootScope.testAdmin;

        $scope.definition = {};
        $scope.regexNames=[];
        $scope.spider = {};

        function load(){
            $scope.processing=true;
            Spiders.get($routeParams.spiderID).then(function(s){
                $scope.spider =s;
                Spiders.getRegexURLGroups().then(function(g){
                    $scope.regexURLGroups = g;
                    Spiders.getRegexNames().then(function(n){

                        for(var i in $scope.regexURLGroups){
                            $scope.regexNames[$scope.regexURLGroups[i].id] = [];
                            for(var j in n){
                                if(n[j].regexGroupID == $scope.regexURLGroups[i].id){
                                    $scope.regexNames[$scope.regexURLGroups[i].id].push(n[j]);
                                }
                            }
                        }
                        for(var j in n){
                            $scope.definition[n[j].id] = {};
                            if(n[j].required == true){
                                $scope.definition[n[j].id]['required']=true;
                                $scope.definition[n[j].id]['validateJob']=true;
                            }
                            else{
                                $scope.definition[n[j].id]['required']=false;
                                $scope.definition[n[j].id]['validateJob']=false;
                            }
                        }

                        Spiders.getRegexDefinition($routeParams.spiderID).then(function(sc){
                            for(var i in sc){
                                $scope.definition[sc[i].regexNameID]['id'] = sc[i].id;
                                $scope.definition[sc[i].regexNameID]['required'] = true;
                                $scope.definition[sc[i].regexNameID]['validateJob'] = sc[i].validateJob;
                                if(sc[i].configStatusID == null){
                                    $scope.definition[sc[i].regexNameID]['status'] = 0;
                                }
                                else{
                                    $scope.definition[sc[i].regexNameID]['status'] = sc[i].configStatusID;
                                }
                            }
                            $scope.processing=false;

                        });

                        Spiders.getGroupValidation($scope.spider.id).then(function(v){
                            $scope.available = v;
                            console.debug(v);
                        });
                    });
                });
            });
        }
        load();

        $scope.populate = function(){
            $scope.processing=true;
            Spiders.populateLinks($scope.spider.id).then(function(r){
                Spiders.getGroupValidation($scope.spider.id).then(function(v){
                    $scope.available = v;
                    $scope.processing=false;
                });
            });
        };

        $scope.save =function(){
            $scope.processing=true;
            Spiders.setRegexNames($routeParams.spiderID, $scope.definition).then(function(){
                $scope.processing=false;
                load();
            });
        };

        $scope.admin = function(regexID){
            $location.path("/managerRegexes/"+$routeParams.spiderID+"/"+regexID);
        }

};