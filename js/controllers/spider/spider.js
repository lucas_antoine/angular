'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.Cspiders', []);

//app.controller('spider', ['$scope', '$rootScope', 'Spiders'
//            , function($scope, $rootScope, Spiders) {
controllers.spider = function($scope, $rootScope, Spiders) {
    $rootScope.$broadcast('setCurrent', 'spiders');
    $rootScope.$broadcast('setCurrentU', 'spiderreport');

    $scope.processing = false;

    $scope.testAdmin = $rootScope.testAdmin;

    $scope.spiders = [];

    function load() {
        $scope.processing = true;
        Spiders.getAll().then(function(l) {
            $scope.spiders = l;
            $scope.processing = false;
        });
    }
    load();



    $scope.save = function(l) {
        $scope.processing = true;
        Spiders.doSave(l).then(function() {
            $scope.processing = false;
        });
    }


    $scope.create = function(l) {
        $scope.processing = true;
        Spiders.doCreate(l).then(function() {
            $scope.processing = false;
            load();
        });
    }


    ////////////////////////////////////SPIDER STATUS
    $scope.searchstatus = "ok";
    $scope.jobstatus = "ok";

    $scope.searchFinishDate = "24H ago";
    $scope.jobFinishDate = "4H ago";


    setInterval(function() {
        /*Spiders.getStatus().then(function(d) {
         
         
         });*/
    }, 10000);

};
