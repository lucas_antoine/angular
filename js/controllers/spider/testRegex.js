'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.CspidersTestRegex', []);

//app.controller('testRegex', ['$scope', '$rootScope', '$routeParams', '$location', 'Spiders'
//        , function($scope, $rootScope, $routeParams, $location, Spiders) {
controllers.testRegex = function($scope, $rootScope, $routeParams, $location, Spiders) {

    $rootScope.$broadcast('setCurrent', 'spider');
    //$rootScope.$broadcast('setCurrentW', 'regexDefinition');
    $rootScope.$broadcast('setCurrentSpiderID', $routeParams.spiderID);

    var status_invalid = 2;
    var status_valid = 3;
    var status_curl_fail = 4;
    var status_curl_pass = 5;
    
    

    $scope.spiderConfigID = $routeParams.spiderConfigID;
    $scope.testAdmin = $rootScope.testAdmin;

    $scope.processing = true;
    $scope.passData = {};

    Spiders.get($routeParams.spiderID).then(function(s) {
        $scope.spider = s;
        Spiders.getSpiderConfig($scope.spiderConfigID).then(function(sc) {
            $scope.spiderConfig = sc;
            
            $scope.spiderConfig.configStatusID = status_curl_fail;
            Spiders.saveSpiderConfig($scope.spiderConfig).then(function() {
                console.debug($scope.spiderConfig);
                $scope.regexName = $scope.spiderConfig.regexNames.name;
                $scope.groupName = $scope.spiderConfig.regexNames.regexURLGroups.name;

                Spiders.testSpiderConfig($scope.spiderConfigID).then(function(result) {
                    console.debug(result);
                    $scope.testData = result;
                    $scope.nbResults = 0;
                    for (var i in result.curlSuccess) {
                        $scope.nbResults++;
                    }
                    $scope.processing = false;
                });
            });
            
        });
    });

    $scope.setFail = function() {
        $scope.processing = true;
        $scope.spiderConfig.status = status_curl_fail;
        Spiders.saveSpiderConfig($scope.spiderConfig).then(function() {
            $scope.processing = false;
            //redirect
            $location.path("/managerRegexDefinition/" + $routeParams.spiderID);
        });
    };

    $scope.setPass = function(url) {
        $scope.passData[url] = true;
        var nbPass = 0;
        for (var i in $scope.passData) {
            if ($scope.passData[i] == true) {
                nbPass++;
            }
        }
        
        if (nbPass == $scope.nbResults) {
            console.debug("setting pass");
            $scope.processing = true;
            $scope.spiderConfig.configStatusID = status_curl_pass;
            console.debug($scope.spiderConfig);
            Spiders.saveSpiderConfig($scope.spiderConfig).then(function(data) {
                $scope.processing = false;
                console.debug(data);
                //redirect
                $location.path("/managerRegexDefinition/" + $routeParams.spiderID);
            });
        }        
    };

};

