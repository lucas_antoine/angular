'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.CspidersSearchUrlBuilder', []);

//app.controller('searchUrlBuilder', ['$scope', '$rootScope', '$routeParams', 'Spiders','SearchPOST'
//            , function($scope, $rootScope, $routeParams, Spiders, SearchPOST) {
controllers.searchUrlBuilder = function($scope, $rootScope, $routeParams, Spiders, SearchPOST) {

    $rootScope.$broadcast('setCurrent', 'spider');
    $rootScope.$broadcast('setCurrentW', 'searchUrlBuilder');
    $rootScope.$broadcast('setCurrentSpiderID', $routeParams.spiderID);



    $scope.testAdmin = $rootScope.testAdmin;
    $scope.urlBuilder = [];
    $scope.POST = [];
    $scope.newUrlComponent = [];
    $scope.newPOST = {};


    $scope.searchURLs = [];
    $scope.urlGenerated = false;
    $scope.curlTesting = false;
    $scope.curlTestData = {};
    $scope.urlBuilderStatus = null;



    $scope.updateStatus = function() {
        Spiders.getStatus($routeParams.spiderID).then(function(s) {
            $scope.urlBuilderStatus = s.urlBuilderStatus;
        });
    };
    $scope.setStatus = function(status) {
        return Spiders.setUrlBuilderStatus($routeParams.spiderID, status).then(function() {
            $scope.urlBuilderStatus = status;
            $rootScope.$broadcast('loadSpiderStatus');
        });
    };

    $scope.passGenerated = function(pass) {
        if (!pass) {
            $scope.urlGenerated = false;
            $scope.searchURLs = [];

            $scope.urlBuilderStatus = 1;
            Spiders.setUrlBuilderStatus($routeParams.spiderID, $scope.urlBuilderStatus);
        }
        else {
            $scope.processing = true;
            $scope.urlBuilderStatus = 4;
            Spiders.setUrlBuilderStatus($routeParams.spiderID, $scope.urlBuilderStatus).then(function() {
                Spiders.saveSearchURLs($routeParams.spiderID).then(function() {
                    $rootScope.$broadcast('loadSpiderStatus');
                    $scope.processing = false;
                });
            });
        }
    };



    $scope.sortableOptions = {
        update: function(e, ui) {

        },
        stop: function(e, ui) {
            $scope.$apply(function() {
                $scope.processing = true;
                Spiders.setUrlBuilder($routeParams.spiderID, $scope.urlBuilder).then(function() {
                    Spiders.setUrlBuilderStatus($routeParams.spiderID, 2).then(function() {
                        $rootScope.$broadcast('loadSpiderStatus');
                        $scope.urlBuilderStatus = 2;
                        $scope.urlGenerated = false;
                        $scope.searchURLs = false;
                        $scope.processing = false;
                    });
                });
            });
        },
        axis: 'y'
    };

    /*$('#sortable').sortable().bind('sortupdate', function() {
     sort($(this));
     });*/

    $scope.processing = true;
    Spiders.get($routeParams.spiderID).then(function(s) {
        $scope.spider = s;
        SearchPOST.getAll(s.id).success(function(l) {
            $scope.POST = l;
        });
        Spiders.getPreferences($routeParams.spiderID).then(function(s) {
            $scope.preferences = s;
            Spiders.getUrlBuilder($routeParams.spiderID).then(function(s) {
                $scope.urlBuilder = s;
                Spiders.getImportedTypes($routeParams.spiderID).then(function(i) {
                    $scope.importedTypes = i;
                    Spiders.getSearchURLs($routeParams.spiderID).then(function(s) {
                        $scope.searchURLs = s;
                        Spiders.getStatus($routeParams.spiderID).then(function(s) {
                            $scope.urlBuilderStatus = s.urlBuilderStatus;
                            if ($scope.urlBuilderStatus == 5) {
                                $scope.timer = setInterval(function() {
                                    Spiders.getUrlBuilderTestCurl($routeParams.spiderID).then(function(r) {
                                        $scope.curlTestData = r;
                                        if ((r['NbTestedURL'] + r['404Errors']) == r['totlaURLs']) {
                                            clearInterval($scope.timer);
                                            $rootScope.$broadcast('loadSpiderStatus');
                                        }
                                    });
                                }, 2000);
                            }
                            else {
                                $scope.processing = false;
                            }
                        });
                    });
                });

            });
        });
    });



    $scope.savePreferences = function() {
        Spiders.savePreferences($scope.preferences);
    };

    $scope.addPOST = function() {
        $scope.processing = true;
        $scope.newPOST.spiderID = $scope.spider.id;
        console.debug($scope.newPOST);
        $scope.newUrlComponent = [];
        $scope.newPOST.dataName = $scope.newPOST.name;
        SearchPOST.doCreate($scope.newPOST).success(function(np) {
            $scope.POST.push(np);
            $scope.newPOSt = {};
            $scope.processing = false;
        });
    };



    $scope.removePOST = function(p) {
        $scope.processing = true;
        SearchPOST.doDelete(p.id).success(function() {
            SearchPOST.getAll($scope.spider.id).success(function(l) {
                $scope.POST = l;
                $scope.processing = false;
            });
        });
    };

    $scope.addUrlComponent = function() {
        $scope.processing = true;
        $scope.urlBuilder.push($scope.newUrlComponent);
        $scope.newUrlComponent = [];
        console.debug($scope.newUrlComponent);
        console.debug($scope.urlBuilder);
        Spiders.setUrlBuilder($routeParams.spiderID, $scope.urlBuilder).then(function() {
            $scope.processing = false;
        });
    };
    $scope.removePart = function(u) {
        $scope.processing = true;
        console.debug(u);
        for (var i in $scope.urlBuilder) {
            if ($scope.urlBuilder[i] == u) {
                delete($scope.urlBuilder[i]);
            }
        }
        Spiders.removeUrlPart(u).then(function() {
            Spiders.getUrlBuilder($routeParams.spiderID).then(function(s) {
                $scope.urlBuilder = s;
                $scope.processing = false;
            });

        });
    };

    $scope.generate = function() {
        $scope.processing = true;
        $scope.urlGenerated = false;

        Spiders.setUrlBuilderStatus($routeParams.spiderID, 1).then(function() {
            $scope.urlBuilderStatus = 1;
            Spiders.generateSearchURLs($routeParams.spiderID).then(function(s) {
                $scope.searchURLs = s;
                $scope.urlGenerated = true;
                $scope.processing = false;
                $scope.generatingURLs = false;
                $scope.updateStatus();
                $rootScope.$broadcast('loadSpiderStatus');
            });

        });
    };



    $scope.testCurl = function() {
        $scope.processing = true;
        $scope.curlTesting = true;
        $scope.setStatus(5);

        $scope.timer = setInterval(function() {
            Spiders.getUrlBuilderTestCurl($routeParams.spiderID).then(function(r) {
                $scope.curlTestData = r;
            });
        }, 4000);

        Spiders.testUrlBuilderCurl($routeParams.spiderID).then(function(r) {
            clearInterval($scope.timer);
            Spiders.getUrlBuilderTestCurl($routeParams.spiderID).then(function(r) {
                $scope.curlTestData = r;
                $scope.updateStatus();
                $rootScope.$broadcast('loadSpiderStatus');
                $scope.processing = false;
            });
        });
    };
};