'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.CspidersRegexes', []);

//app.controller('regexes', ['$scope', '$rootScope','$routeParams', '$location','Spiders'
//            , function($scope, $rootScope, $routeParams, $location, Spiders) {
controllers.regexes = function($scope, $rootScope, $routeParams, $location, Spiders) {
    $rootScope.$broadcast('setCurrent', 'spider');
    $rootScope.$broadcast('setCurrentW', 'regexes');
    $rootScope.$broadcast('setCurrentSpiderID', $routeParams.spiderID);

    

    $scope.spiderConfigID = $routeParams.regexID;

    $scope.processing = false;
    $scope.testAdmin = $rootScope.testAdmin;

    Spiders.getRegexTypes().then(function(t) {
        $scope.regexTypes = t;
        load();
    });

    function load() {
        $scope.processing = true;
        Spiders.getSpiderConfig($scope.spiderConfigID).then(function(d) {
            $scope.spiderConfig = d;
            Spiders.loadRegexes($scope.spiderConfigID).then(function(r) {
                $scope.regexes = r;
                console.debug(r);
                $scope.processing = false;
            });
        });
    }

    $scope.add = function(newregex) {
        newregex.spiderConfigID = $scope.spiderConfigID;
        $scope.processing = true;
        Spiders.addRegex($scope.spiderConfigID, newregex).then(function(c) {
            $scope.processing = false;
            load();
        });
    };

    $scope.edit = function(regex) {
        $scope.processing = true;
        Spiders.editRegex(regex).then(function() {
            $scope.processing = false;
            load();
        });
    };

    $scope.remove = function(regex) {
        $scope.processing = true;
        Spiders.removeRegex(regex).then(function() {
            $scope.processing = false;
            load();
        });
    };

    $scope.test = function() {
        $location.path('/testRegex/' + $routeParams.spiderID + '/' + $scope.spiderConfigID);
    };




};