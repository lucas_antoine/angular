'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.Cattributes', []);

//app.controller('literals',['$scope',  '$rootScope', 'Literals'
//    , function($scope,  $rootScope, Literals){
controllers.literals = function($scope,  $rootScope, Literals){
    $rootScope.$broadcast('setCurrent','urls');
    $rootScope.$broadcast('setCurrentU','literals');

    $scope.processing=false;

    $scope.testAdmin = $rootScope.testAdmin;

    function load(){
        $scope.processing=true;
        $scope.nl = {name : "",type : ""};
        Literals.getAll().then(function(l){
            $scope.literals = l;
            console.debug(l);
            $scope.processing=false;
        });
    }
    load();



    $scope.save = function(l){
        $scope.processing=true;
        Literals.doSave(l).then(function(){
            $scope.processing=false;
        });
    }


    $scope.create = function(l){
        $scope.processing=true;
        Literals.doCreate(l).then(function(){
            $scope.processing=false;
            load();
        });
    }

};


//app.controller('attributes', ['$scope', '$rootScope', '$location', '$routeParams', 'Paths', 'ObjectData', 'Objects', 'Attributes'
//, function($scope, $rootScope, $location, $routeParams, Paths, ObjectData, Objects, Attributes){
controllers.attributes = function($scope, $rootScope, $location, $routeParams, Paths, ObjectData, Objects, Attributes){
    $rootScope.$broadcast('setCurrent','urls');
    $rootScope.$broadcast('setCurrentU','attributes');

    $scope.testAdmin = $rootScope.testAdmin;


    $scope.attributeChoice=[];
    $scope.processing=false;
    $scope.na = {};

    $scope.paths = [];
    var adler = function(a){for(var b=65521,c=1,d=0,e=0,f;f=a.charCodeAt(e++);d=(d+c)%b)c=(c+f)%b;return(d<<16)|c};

    function load(){

        $scope.processing=true;
        Paths.get().then(function(p){
            $scope.paths = p;
            Attributes.getAll().then(function(a){
                $scope.attributes = a;

                for(var i in a){
                    $scope.update(a[i]);
                }
                $scope.processing=false;
            });
        });
    }

    load();

    $scope.update = function(a){

        $scope.processing=true;
        /*if (typeof $scope.attributeChoice[a.path] == 'undefined') {
            console.debug("updating "+a.path);*/
            ObjectData.get(encodeURIComponent(a.path)).then(function(d){
                $scope.attributeChoice[a.path]=d;
                $scope.processing=false;
            });
        /*}
        else{
             $scope.processing=false;
        }*/
        $scope.setAdler(a);
    }
    $scope.setAdler = function(a){
        if(a!=null){
            var p = (a.path != 'undefined') ? a.path : "";
            var t = (a.attributeType != 'undefined') ? a.attributeType : "";
            var n = (a.attributeName != 'undefined') ? a.attributeName : "";

            var aa = adler("a"+p+t+n);

            a.urlName = "P" + aa;
        }
        else{
            var p = ($scope.na.path != 'undefined') ? $scope.na.path : "";
            var t = ($scope.na.attributeType != 'undefined') ? $scope.na.attributeType : "";
            var n = ($scope.na.attributeName != 'undefined') ? $scope.na.attributeName : "";

            var a = adler("a"+p+t+n);

            $scope.na.urlName = "P" + a;
        }
    }
    $scope.save = function(a){
        $scope.processing=true;
        Attributes.doSave(a).then(function(){
            $scope.processing=false;
        });
    };
    $scope.delete = function(a){
        $scope.processing=true;
        Attributes.doDelete(a).then(function(){
            $scope.processing=false;
            load();
        });
    };
    $scope.create = function(a){
        $scope.processing=true;
        Attributes.doCreate(a).then(function(){
            $scope.processing=false;
            load();
            $scope.na.path='';
            $scope.na.attributeType='';

        });
    }

};

