'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.CurlParams', []);




//app.controller('urlParams', ['$scope', '$rootScope', '$routeParams', '$window', 'Constructors', 'Instanciators', 'Attributes', 'Literals', 'Items', 'UrlBuilding', 'Urls', 'UrlLiterals', 'Gets',
//        function($scope, $rootScope, $routeParams, $window, Constructors, Instanciators, Attributes, Literals, Items, UrlBuilding, Urls, UrlLiterals, Gets){
controllers.urlParams = function($scope, $rootScope, $routeParams, $window, Constructors, Instanciators, Attributes, Literals, Items, UrlBuilding, Urls, UrlLiterals, Gets){
    
    $scope.processing = false;
    $scope.urlID = $routeParams.urlID;
    $scope.active="params";
    $scope.testAdmin = $rootScope.testAdmin;


    Urls.get($scope.urlID).then(function(u){
        $scope.url = u;
        load();
    });

    function load(){
        $scope.processing = true;
        $scope.choices=[];

        Urls.getPossibleUrlData($scope.urlID).then(function(pd) {
            $scope.possibleUrlData = pd;
            console.debug(pd);
            Attributes.getAll().then(function(a){

                for(var i in a){
                    a[i].longName = a[i].path+"->";
                    if(a[i].attributeType=="property"){
                        a[i].shortName ="$" +  a[i].attributeName;
                    }
                    else{
                        a[i].shortName = a[i].attributeName+"()";
                    }
                    a[i].longName+=a[i].shortName;
                    a[i].type="object";
                    $scope.choices.push(a[i]);
                }
                $scope.attributes = a;
                Literals.getAll().then(function(l){
                    $scope.literals = [];
                    for(var i in l){
                        l[i].longName = l[i].name + " ["+l[i].type+"]";
                        l[i].type="literal";
                        $scope.choices.push(l[i]);
                        $scope.literals.push(l[i]);
                    }

                    UrlLiterals.getAll($scope.urlID).then(function(ul){
                        $scope.urlLiterals = ul;

                        UrlBuilding.getChoices($scope.urlID , "constructor").then(function(c){
                            $scope.availableConstructors = c;
                             UrlBuilding.getChoices($scope.urlID , "literal").then(function(c){
                                $scope.availableLiterals = c;
                                UrlBuilding.getChoices($scope.urlID, "object").then(function(c){
                                    $scope.availableObjects = c;
                                    Instanciators.getAll().then(function(inn){
                                        for(var i in inn){
                                            var p = inn[i].path;
                                            inn[i].choices = [];
                                            for(var j in $scope.choices){
                                                if($scope.choices[j].path == p){
                                                    inn[i].choices.push($scope.choices[j]);
                                                }
                                            }
                                        }
                                        $scope.instanciators = inn;
                                        Constructors.getAll($scope.urlID).then(function(c){
                                            $scope.constructors = [];
                                            for(var i in c){

                                                var instID = c[i].instanciatorID;
                                                for(var j in $scope.instanciators){
                                                    if($scope.instanciators[j].id == instID){
                                                        c[i].instData = $scope.instanciators[j];
                                                    }
                                                }
                                                c[i].possibleAttributes = [];

                                                for(var j in c[i].instData.instanciatorsAttributess){
                                                    var aID = c[i].instData.instanciatorsAttributess[j].attributeID;
                                                    for(var k in $scope.attributes){
                                                        if($scope.attributes[k].id == aID){
                                                            c[i].possibleAttributes.push($scope.attributes[k]);
                                                        }
                                                    }
                                                }

                                            }
                                            $scope.constructors = c;
                                            Gets.getAll($scope.urlID).then(function(g){

                                                for(var i in g){
                                                    for(var j in $scope.constructors){
                                                        if($scope.constructors[j].id == g[i].constructorID){
                                                            g[i].constructor = $scope.constructors[j];
                                                            g[i].possibleAttrs = $scope.constructors[j].possibleAttributes;
                                                        }
                                                    }
                                                }
                                                $scope.gets = g;
                                                console.debug($scope.gets);
                                                loadItems();
                                                setPossibleAttributes();                                                
                                                console.debug("initiating sortable");
                                                console.debug($('#sortable').html());
                                                $('#sortable').sortable().bind('sortupdate', function() {
                                                    sort($(this));
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });

                });
            });
        });
    }

    $scope.updateGetConstructor = function(g){

        for(var j in $scope.constructors){
            if($scope.constructors[j].id == g.constructorID){
                g.constructor = $scope.constructors[j];
                g.possibleAttrs = $scope.constructors[j].possibleAttributes;
            }
        }


    };

    $scope.createG = function(ng){
        $scope.processing = true;
        Gets.doCreate(ng).then(function(c){

            load();
        });
    };
    $scope.saveG = function(ng){
        $scope.processing = true;
        Gets.doSave(ng).then(function(c){
           load();
        });
    };
    $scope.deleteG = function(ng){
        $scope.processing = true;
        Gets.doDelete(ng).then(function(c){
           load();
        });
    };

    $scope.updateChoices = function(nc){
        nc.choices = [];
        var inID = nc.instanciatorID;
        var path = null;
        for(var i in $scope.instanciators){
            console.debug($scope.instanciators[i].id);
            if($scope.instanciators[i].id == inID){
                path = $scope.instanciators[i].path;
            }
        }
        if(path!==null){
            for(var j in $scope.choices){
                if($scope.choices[j].path == path){
                    nc.choices.push($scope.choices[j]);
                }
            }
        }
    };

    function loadItems(){
        $scope.processing = true;
        Items.getAll($scope.urlID).then(function(i){
            $scope.items = i;
            $scope.processing=false;
        });
    }

    $scope.createC = function(c){
        $scope.processing=true;
        c.urlID = $scope.urlID;
        Constructors.doCreate(c).then(function(){
            load();
        });
    };
    $scope.saveC = function(c){
        $scope.processing=true;
        c.urlID = $scope.urlID;
        Constructors.doSave(c).then(function(){
            load();
        });
    };
    $scope.deleteC = function(c){
        $scope.processing=true;
        Constructors.doDelete(c).then(function(){
            load();
        });
    };


    $scope.addL = function(l){
        $scope.processing=true;
        l.urlID = $scope.urlID;
        UrlLiterals.doCreate(l).then(function(){
            load();
        });
    };
    $scope.saveL = function(l){
        $scope.processing=true;
        l.urlID = $scope.urlID;
        UrlLiterals.doSave(l).then(function(){
            load();
        });
    };
    $scope.deleteL = function(l){
        $scope.processing=true;
        l.urlID = $scope.urlID;
        UrlLiterals.doDelete(l).then(function(){
            load();
        });
    };

    $scope.clear = function(nu){
        nu = {type : nu.type};
    };

    function setPossibleAttributes(){

        $scope.possibleAttrs = [];


        for(var i in $scope.constructors){

            var index = $scope.constructors[i].instData.path + $scope.constructors[i].instData.functionName;
            $scope.possibleAttrs[index] = $scope.constructors[i].possibleAttributes;

        }

    }

    $scope.setPossibleAttributes = function(nu){

        $scope.possibleAttributes = [];
        for(var i in $scope.constructors){
            if($scope.constructors[i].id == nu.constructorID){

                $scope.possibleAttributes = $scope.constructors[i].possibleAttributes;
            }
        }
    };




    $scope.createI = function(p){
        $scope.processing=true;
        p.urlID = $scope.urlID;
        Items.doCreate(p).then(function(){
           loadItems();
        });
    };

    $scope.saveI = function(p){
        $scope.processing=true;
        p.urlID = $scope.urlID;
        Items.doSave(p).then(function(){
           loadItems();
        });
    };

    $scope.deleteI = function(p){
        $scope.processing=true;
        Items.doDelete(p).then(function(){
           loadItems();
        });
    };

    function loadPossibleAttributes(ub){
        for(var i in ub){
            ub[i].possibleAttributes = [];
            var cID = ub[i].constructorID;
            for(var j in $scope.constructors){
                if($scope.constructors[j].id==cID){
                    ub[i].possibleAttributes = $scope.constructors[j].possibleAttributes;
                }
            }
        }
        console.debug(ub);
        return ub;
    }

    $scope.initialData = function(u){
        u.constructorID=0;
        u.attributeID=0;
        u.constValue="";
    };

    $scope.updatePossibleAttributes = function(){
        $scope.urls = loadPossibleAttributes($scope.urls);
    };

    $scope.manageI = function(p){
        $scope.processing = true;
        $scope.currentItem = p;
        $scope.active="ub";

        UrlBuilding.getAll(p.id).then(function(u){
            $scope.urls = loadPossibleAttributes(u);
            sort($("#sortable"));
            $scope.processing=false;

        });
    };

    $scope.createUB = function(nu){
        $scope.processing = true;
        nu.itemID = $scope.currentItem.id;
        nu.order = $("#sortable li").length +1;
        UrlBuilding.doCreate(nu).then(function(){
            $scope.nu = {};
            $scope.manageI($scope.currentItem);
        });
    };

    $scope.saveUB = function(u){
        $scope.processing = true;
        u.itemID = $scope.currentItem.id;
        UrlBuilding.doSave(u).then(function(){
            $scope.manageI($scope.currentItem);
        });
    };
    $scope.deleteUB = function(u){
        $scope.processing = true;
        UrlBuilding.doDelete(u).then(function(){
            $scope.manageI($scope.currentItem);
        });
    };

    function sort(UL){
        var order = [];
        UL.find("li").each(function(li){
            order.push($(this).attr("ubid"));
        });
        UrlBuilding.setOrder($scope.currentItem.id, order);
    }
    
    

};
