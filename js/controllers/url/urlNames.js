'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.CurlNames', []);

//app.controller('urlNames', ['$scope', '$rootScope', '$routeParams', '$window', 'UrlNames', function($scope, $rootScope, $routeParams, $window, UrlNames){

controllers.urlNames = function($scope, $rootScope, $routeParams, $window, UrlNames){
    
    $rootScope.$broadcast('setCurrent','urls');
    $rootScope.$broadcast('setCurrentU','urlNames');

    $scope.adminTest = false;


    $scope.processing = false;

    function load(){
        $scope.processing=true;
        UrlNames.getAll2().then(function(un){
            $scope.unames = un;
            $scope.processing = false;
        });
    }

    load();

    $scope.save = function(un){
        $scope.processing=true;
        UrlNames.doSave(un).then(function(){
            $scope.processing=false;
        });
    };

    $scope.create = function(un){
        $scope.processing=true;
        UrlNames.doCreate(un).then(function(){
            $scope.u={};
            load();
        });
    };

    $scope.delete = function(un){
        $scope.processing=true;
        UrlNames.doDelete(un).then(function(){
            load();
        });
    };

};