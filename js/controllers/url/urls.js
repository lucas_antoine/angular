'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.Curl', []);




//app.controller('adminUrls', ['$scope', '$rootScope', '$location', '$routeParams', 'Urls', 'Modules', 'Actions', 'Methods','Options', 'UrlNames',
//        function($scope, $rootScope, $location, $routeParams, Urls, Modules, Actions, Methods,Options, UrlNames){
controllers.adminUrls = function($scope, $rootScope, $location, $routeParams, Urls, Modules, Actions, Methods, Options, UrlNames) {

    $rootScope.$broadcast('setCurrent', 'websites');
    $rootScope.$broadcast('setCurrentW', 'urls');
    $rootScope.$broadcast('setCurrentWebsiteID', $routeParams.websiteID);
    $scope.testAdmin = $rootScope.testAdmin;

    $scope.actionChoices = [];
    $scope.methodChoices = [];
    $scope.processing = false;
    $scope.websiteID = $routeParams.websiteID;


    function load() {
        $scope.processing = true;

        Methods.superGetAll().then(function(m) {
            $scope.modules = m;

            Urls.getAll($scope.websiteID).then(function(r) {
                $scope.urls = r;

                Options.get($scope.websiteID).then(function(o) {
                    $scope.defaultUrlID = o.defaultUrlID;
                    UrlNames.getAll2().then(function(n) {
                        $scope.urlNames = n;
                        $scope.processing = false;
                        console.debug("modules : ");
                        console.debug($scope.modules);
                        console.debug("urlNames : ");
                        console.debug($scope.urlNames);
                        console.debug("urls : ");
                        console.debug($scope.urls);
                    });
                });
            });
        });


    }

    load();

    $scope.setDefaultUrl = function(url) {
        $scope.processing = true;
        alert(url.id);
        Options.setDefaultUrl(url.id, $scope.websiteID).then(function() {
            $scope.defaultUrlID = url.id;
            $scope.processing = false;
        });
    };

    $scope.updateA = function(url) {
        $scope.processing = true;
        if (url.urlType === undefined) {
            url.urlType = 'page';
        }
        var t = url.urlType=='ajax' ? 1 : 0;
        url.possibleActions = $scope.modules[url.moduleName][t];
        $scope.updateM(url);
    };
    $scope.updateM = function(url) {
        $scope.processing = true;
        if (url.urlType === undefined) {
            url.urlType = 'page';
        }
        var t = url.urlType=='ajax' ? 1 : 0;
        url.possibleMethods = $scope.modules[url.moduleName][t][url.action];
        console.debug("modules : ");
        console.debug(url);
        console.debug($scope.modules);
        $scope.processing = false;
    };

    $scope.save = function(url) {
        $scope.processing = true;
        url.websiteID = $scope.websiteID;
        Urls.doSave(url).then(function() {
            $scope.processing = false;
        });
    };

    $scope.create = function(url) {
        console.debug(url);
        $scope.processing = true;
        url.websiteID = $scope.websiteID;
        Urls.doCreate(url).then(function() {
            load();
            $scope.nurl.moduleName = "";
            $scope.nurl.ajax = 0;
            $scope.nurl.urlName = "";
            $scope.nurl.action = null;
            $scope.nurl.method = null;

            $scope.processing = false;
        });
    };

    $scope.manage = function(url) {
        $location.path("/manageUrl/" + url.id);
    };

};

