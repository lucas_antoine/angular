'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.Chtaccess', []);




//app.controller('htaccess', ['$scope', '$rootScope', '$routeParams', 'Options', 'HtAccess',function($scope, $rootScope, $routeParams, Options, HtAccess){
controllers.htaccess = function($scope, $rootScope, $routeParams, Options, HtAccess) {

    $rootScope.$broadcast('setCurrent', 'websites');
    $rootScope.$broadcast('setCurrentW', 'htaccess');
    $rootScope.$broadcast('setCurrentWebsiteID', $routeParams.websiteID);
    $scope.testAdmin = $rootScope.testAdmin;
    $scope.currentView = 'live';

    $scope.websiteID = $routeParams.websiteID;

    $scope.processing = true;

    function load() {
        Options.get($scope.websiteID).then(function(o) {

            if (o.lastHtAccessGenerationDate === null) {
                o.lastHtAccessGenerationDate = "Never";
            }
            $scope.options = o;

            $scope.reload();

        });
    }

    load();

    function loadLive() {
        HtAccess.getProd($scope.websiteID).then(function(p) {
            $scope.htprod = p;
        });
    }
    function loadGenerated() {

        HtAccess.getDev($scope.websiteID).then(function(p) {
            $scope.htdev = p;
        });
    }
    $scope.overwrite = function() {
        HtAccess.setProd($scope.websiteID, $scope.htdev).then(function() {
            load();
        });
    };
    $scope.changeHtAccess = function(view) {
        $scope.currentView = view;
    };

    $scope.reload = function() {
        HtAccess.getDev($scope.websiteID).then(function(p) {
            $scope.htdev = p;
            HtAccess.getProd($scope.websiteID).then(function(p) {
                $scope.htprod = p;
                $scope.diffUsingJS();
                $scope.processing = false;
            });
        });
    }
    //DIFF
    $scope.viewType = 1;// set 1 for inline, 0 for not inline
    $scope.diffUsingJS = function() {
        $scope.changeHtAccess('diff')
        var base = difflib.stringAsLines($scope.htprod);
        var newtxt = difflib.stringAsLines($scope.htdev);
        var sm = new difflib.SequenceMatcher(base, newtxt);
        var opcodes = sm.get_opcodes();

        //var diffoutputdiv = $("diffoutput");
        //while (diffoutputdiv.firstChild) diffoutputdiv.removeChild(diffoutputdiv.firstChild);

        var option = {
            baseTextLines: base,
            newTextLines: newtxt,
            opcodes: opcodes,
            baseTextName: "Production",
            newTextName: "Generated",
            contextSize: null,
            viewType: $scope.viewType
        };
        var data = diffview.buildView(option);
        $("#diffDIV").html(data);

    }
};