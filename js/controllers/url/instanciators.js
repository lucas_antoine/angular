'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.Cinstanciators', []);

//app.controller('instanciators',['$scope',  '$rootScope', 'Instanciators','Attributes', function($scope,  $rootScope, Instanciators,Attributes){
controllers.instanciators = function($scope,  $rootScope, Instanciators,Attributes){

    $rootScope.$broadcast('setCurrent','urls');
    $rootScope.$broadcast('setCurrentU','instanciators');


    $scope.processing=false;
    $scope.paths = [];
    $scope.availableAttributes = [];
    $scope.ni = {attributes : []};
    $scope.loaded = false;

    function inArray(needle, haystack) {
        var length = haystack.length;
        for(var i = 0; i < length; i++) {
            if(haystack[i] == needle) return true;
        }
        return false;
    }

    function load(){
        $scope.processing=true;

        Instanciators.getErrors().then(function(e){
            var e = e;
               Instanciators.getAll().then(function(i){
                    $scope.instanciators = i;

                    for(var i in $scope.instanciators){
                        var id = $scope.instanciators[i].id;
                        $scope.instanciators[i].error = e[id];
                    }

                    if($scope.loaded === false){
                        Attributes.getAll().then(function(a){
                            $scope.paths = [];
                            $scope.availableAttributes = [];
                            for(var i in a){
                                if(a[i].attributeType=="method"){
                                    a[i].longName = a[i].attributeName+"()";
                                }
                                else{
                                    a[i].longName = "$"+a[i].attributeName;
                                }
                            }
                            $scope.attributes = a;

                            for(var i in a){
                                if(!inArray(a[i].path, $scope.paths)){
                                    $scope.paths.push(a[i].path);
                                }
                            }
                            for(var i in $scope.paths){
                                $scope.availableAttributes[$scope.paths[i]] = [];
                                for(var j in $scope.attributes){
                                    if($scope.attributes[j].path == $scope.paths[i]){
                                        $scope.availableAttributes[$scope.paths[i]].push($scope.attributes[j]);
                                    }
                                }
                            }
                            $scope.loaded=true;

                        });
                        $scope.processing=false;
                    }
                });


        });

    }
    load();
    $scope.addAttribute = function(na){

        if($scope.ni.path!=='undefined' && na!==undefined){
            var a = $scope.availableAttributes[$scope.ni.path] [na];
            if(!inArray(a, $scope.ni.attributes)){
                $scope.ni.attributes.push(a);
            }
        }
    };

    $scope.create = function(ni){
        $scope.processing = true;
        if(ni.path!== undefined && ni.attributes.length>0){
            Instanciators.doCreate(ni).then(function(){
                $scope.ni = {attributes : []};
                load();
            });
        }
    };

    $scope.delete = function(i){
        $scope.processing = true;
        Instanciators.doDelete(i).then(function(){
            load();
        });
    }
};

