'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.controllers', []);

//app.controller('adminClassification', ['$scope', '$rootScope', '$routeParams', "Types", 'ClassificationOrigins', 'Classifications',
//    function($scope, $rootScope, $routeParams, Types, ClassificationOrigins, Classifications) {

controllers.adminClassification = function($scope, $rootScope, $routeParams, Types, ClassificationOrigins, Classifications) {
    $rootScope.$broadcast('setCurrent', 'classifications');
    $rootScope.$broadcast('setCurrentC', 'classifications');
    $scope.testAdmin = $rootScope.testAdmin;



    $scope.stats = {};
    $scope.type = "0";
    $scope.origin = "0";
    $scope.adminEnable = false;
    $scope.editEnable = false;

    $scope.lvl0List = {};
    $scope.lvl1List = {};
    $scope.lvl2List = {};
    $scope.lvl3List = {};
    $scope.lvl4List = {};
    $scope.lvl5List = {};

    $scope.lvl0 = 0;
    $scope.lvl1 = 0;
    $scope.lvl2 = 0;
    $scope.lvl3 = 0;
    $scope.lvl4 = 0;
    $scope.lvl5 = 0;

    $scope.new = false;


    load();

    function load() {


        Types.getAll().then(function(types) {
            $scope.types = types;

            if ($routeParams.typeID) {
                $scope.typeID = $routeParams.typeID;
                ClassificationOrigins.getPossible($routeParams.typeID).then(function(co) {
                    $scope.origins = co;
                    $scope.originID = $routeParams.originID;
                    $scope.setOrigin();
                });
            }

        }, function(reason) {
            console.debug(reason);
        });
    }

    function getTOTAL() {



    }

    $scope.edit = function(id) {
        $scope.editEnable = true;
        $scope.new = false;
        Classifications.get(id).then(function(classification) {
            $scope.classification = classification;
            $scope.processing = false;
        }, function(reason) {
            console.debug(reason);
        });
    }



    $scope.update = function(depth) {
        $scope.editEnable = false;
        var id0 = false;

        switch (depth) {
            case 0 :
                id0 = $scope.lvl0 == 0 ? true : false;
                break;
            case 1 :
                id0 = $scope.lvl1 == 0 ? true : false;
                break;
            case 2 :
                id0 = $scope.lvl2 == 0 ? true : false;
                break;
            case 3 :
                id0 = $scope.lvl3 == 0 ? true : false;
                break;
            case 4 :
                id0 = $scope.lvl4 == 0 ? true : false;
                break;
            case 5 :
                id0 = $scope.lvl5 == 0 ? true : false;
                break;
        }

        if (id0) {
            switch (depth) {
                case 0 :
                    $scope.lvl1List = {};
                case 1 :
                    $scope.lvl2List = {};
                case 2 :
                    $scope.lvl3List = {};
                case 3 :
                    $scope.lvl4List = {};
                case 4 :
                    $scope.lvl5List = {};
            }
        }
        else {

            switch (depth) {
                case 0 :
                    $scope.lvl1List = {};
                    $scope.lvl2List = {};
                    $scope.lvl3List = {};
                    $scope.lvl4List = {};
                    $scope.lvl5List = {};
                    var parentID = $scope.lvl0;
                    break;
                case 1 :
                    $scope.lvl2List = {};
                    $scope.lvl3List = {};
                    $scope.lvl4List = {};
                    $scope.lvl5List = {};
                    var parentID = $scope.lvl1;
                    break;
                case 2 :
                    $scope.lvl3List = {};
                    $scope.lvl4List = {};
                    $scope.lvl5List = {};
                    var parentID = $scope.lvl2;
                    break;
                case 3 :
                    $scope.lvl4List = {};
                    $scope.lvl5List = {};
                    var parentID = $scope.lvl3;
                    break;
                case 4 :
                    $scope.lvl5List = {};
                    var parentID = $scope.lvl4;
                    break;

            }

            Classifications.getAll($scope.originID, parentID).then(function(lvl) {
                switch (depth) {
                    case 0 :
                        $scope.lvl1List = lvl;
                        break;
                    case 1 :
                        $scope.lvl2List = lvl;
                        break;
                    case 2 :
                        $scope.lvl3List = lvl;
                        break;
                    case 3 :
                        $scope.lvl4List = lvl;
                        break;
                    case 4 :
                        $scope.lvl5List = lvl;
                        break;
                }
            }, function(reason) {
                console.debug(reason);
            });

        }
    }

    $scope.changeType = function() {
        ClassificationOrigins.getPossible($scope.typeID).then(function(co) {
            $scope.origins = co;
        });



    };

    $scope.setOrigin = function() {

        Classifications.getStats($scope.originID).then(function(stats) {
            $scope.stats = stats;
            $scope.lvl0List = {};
            $scope.lvl1List = {};
            $scope.lvl2List = {};
            $scope.lvl3List = {};
            $scope.lvl4List = {};
            $scope.lvl5List = {};
            Classifications.getAll($scope.originID, 0).then(function(lvl) {
                $scope.lvl0List = lvl;
                console.debug(lvl);
            }, function(reason) {
                console.debug(reason);
            });

        }, function(reason) {
            console.debug(reason);
        });

    };

    $scope.add = function(parentID, depth) {
        $scope.editEnable = true;
        $scope.processing = false;
        $scope.classification = {
            parentID: parentID,
            depth: depth,
            sourceID: $scope.source,
            typeID: $scope.type,
            aliases: "",
            description: "",
            contextID: "",
            url: ""
        };
        $scope.new = true;

    }


    $scope.save = function(s) {
        $scope.processing = true;
        Classifications.doSave(s).then(function() {
            if (s.depth > 0) {
                $scope.update(s.depth - 1);
            }
            else {
                $scope.setOrigin();
            }
            $scope.processing = false;
        }, function(reason) {
            console.debug(reason);
        });
    };

    $scope.create = function(s) {
        $scope.processing = true;

        Classifications.doCreate(s).then(function() {
            if (s.depth > 0) {
                $scope.update(s.depth - 1);
            }
            else {
                $scope.setOrigin();
            }
            $scope.processing = false;
        }, function(reason) {
            console.debug(reason);
        });
    }

    $scope.delete = function(s) {
        $scope.processing = true;
        Classifications.doDelete(s).then(function() {
            if (s.depth > 0) {
                $scope.update(s.depth - 1);
            }
            else {
                $scope.setOrigin();
            }
            $scope.processing = false;
        }, function(reason) {
            console.debug(reason);
        });
    }



};


//app.controller('adminClassificationOrigins', ['$scope', '$rootScope', "Types", 'ClassificationOrigins', 'Origins',
//    function($scope, $rootScope, Types, ClassificationOrigins, Origins) {
controllers.adminClassificationOrigins = function($scope, $rootScope, Types, ClassificationOrigins, Origins) {
    $rootScope.$broadcast('setCurrent', 'classifications');
    $rootScope.$broadcast('setCurrentC', 'sources');
    $scope.testAdmin = $rootScope.testAdmin;


    function load() {
        Types.getAll().then(function(t) {
            $scope.types = t;
            Origins.getAll().success(function(o) {
                $scope.origins = o;
                ClassificationOrigins.getAll().then(function(co) {
                    $scope.classificationOrigins = co;
                });
            });
        });
    }

    load();

    $scope.save = function(co) {
        ClassificationOrigins.doSave(co).then(function() {
            load();
        });
    };

    $scope.create = function(co) {
        ClassificationOrigins.doCreate(co).then(function() {
            load();
        });
    };

    $scope.remove = function(co) {
        ClassificationOrigins.doDelete(co.id).then(function() {
            load();
        });
    };


};

//app.controller('adminType', ['$scope', '$rootScope', 'Types', "ResolutionTypes", 'ClassificationOrigins', 'Websites',
//    function($scope, $rootScope, Types, ResolutionTypes, ClassificationOrigins, Websites) {
controllers.adminType = function($scope, $rootScope, Types, ResolutionTypes, ClassificationOrigins, Websites) {

    $rootScope.$broadcast('setCurrent', 'classifications');
    $rootScope.$broadcast('setCurrentC', 'types');
    $scope.testAdmin = $rootScope.testAdmin;

    $scope.processing = false;
    load();

    function load() {
        ResolutionTypes.getAll().then(function(rt) {
            $scope.resolutionTypes = rt;
        }, function(reason) {
            console.debug(reason);
        });

        Types.getAll().then(function(t) {
            $scope.types = t;
        }, function(reason) {
            console.debug(reason);
        });

        Websites.getAll().then(function(c) {
            var l = Math.round((c.length - 1) / 5) + 1;
            console.debug("l=" + l);
            console.debug("c.length = " + c.length);

            $scope.websites = c;
            $scope.websites0 = [];
            $scope.websites1 = [];
            $scope.websites2 = [];
            $scope.websites3 = [];
            $scope.websites4 = [];

            $scope.selectedWebsites = [];

            for (var i = 1; i < l; i++) {
                $scope.websites0.push(c[i]);
            }
            if (l < c.length) {
                console.debug("1");
                for (var i = l; i < 2 * l; i++) {
                    try {
                        $scope.websites1.push(c[i]);
                    }
                    catch (err) {
                    }
                }
            }
            if (2 * l < c.length) {
                console.debug("2");
                for (var i = 2 * l; i < 3 * l; i++) {
                    try {
                        $scope.websites2.push(c[i]);
                    }
                    catch (err) {
                    }
                }
            }
            if (3 * l < c.length) {
                console.debug("3");
                for (var i = 3 * l; i < 4 * l; i++) {
                    try {
                        $scope.websites3.push(c[i]);
                    }
                    catch (err) {
                    }
                }
            }
            if (4 * l < c.length) {
                console.debug("4");
                for (var i = 4 * l; i <= c.length; i++) {
                    try {
                        $scope.websites4.push(c[i]);
                    }
                    catch (err) {
                    }
                }
            }

        });
    }
    $scope.close = function() {
        $("#url").hide();
    }
    $scope.toggleCheck = function(website) {
        if ($scope.selectedWebsites.indexOf(website) === -1) {
            $scope.selectedWebsites.push(website);
        } else {
            $scope.selectedWebsites.splice($scope.selectedWebsites.indexOf(website), 1);
        }
    };

    $scope.set = function(jq) {
        console.debug(jq.val());
    }
    $scope.select = function(all) {
        if (all) {
            $scope.selectedWebsites = [];
            for (var i = 1; i < $scope.websites.length; i++) {
                $scope.selectedWebsites.push($scope.websites[i]);
            }
            $("input:checkbox").prop('checked', true);
        }
        else {
            $scope.selectedWebsites = [];
            $("input:checkbox").prop('checked', false);
        }
    }

    $scope.save = function(s) {
        $scope.processing = true;
        var promise = Types.doSave(s);
        promise.then(function() {
            load();
            $scope.processing = false;
        }, function(reason) {
            console.debug(reason);
        });

    }

    $scope.remove = function(s) {
        $scope.processing = true;
        var promise = Types.doDelete(s);
        promise.then(function() {
            load();
            $scope.processing = false;
        }, function(reason) {
            console.debug(reason);
        });
    };
    String.prototype.toCamel = function() {
        return this.replace(' ', '');
    };
    $scope.confirm = function() {
        $scope.processing = true;
        $scope.tmpSource.selectedWebsites = $scope.selectedWebsites;
        $scope.tmpSource.constructortype = $scope.constructortype;
        $scope.tmpSource.newwebsite = $scope.newwebsite;
        $scope.tmpSource.defaultValue = $scope.defaultValue;

        Types.doCreate($scope.tmpSource).then(function() {
            load();
            $scope.processing = false;
            $scope.tmpSource.name = "";
            $("#url").hide();
        }, function(reason) {
            console.debug(reason);
        });
    };
    $scope.create = function(source) {
        $scope.tmpSource = source;
        $scope.newwebsite = "no";
        $scope.selectedWebsites = [];
        $scope.cc = $scope.newt.name.toCamel();
        $("#url").show();
    };





};


//app.controller('adminDefaultSource', ['$scope', '$rootScope', 'Types', 'ClassificationOrigins', 'Websites', function($scope, $rootScope, Types, ClassificationOrigins, Websites) {

controllers.adminDefaultSource = function($scope, $rootScope, Types, ClassificationOrigins, Websites) {
    $rootScope.$broadcast('setCurrent', 'classifications');
    $rootScope.$broadcast('setCurrentC', 'websites');
    $scope.testAdmin = $rootScope.testAdmin;

    function load() {
        Types.getAll().then(function(t) {
            $scope.types = t;
            ClassificationOrigins.getAll().then(function(o) {
                $scope.origins = o;
                console.debug(o);
                Websites.getDefaultClassificationOrigins().then(function(s) {
                    console.debug(s);
                    for (var j in $scope.types) {
                        //$scope.types[j].sourceID = 0;
                        for (var i in s) {
                            if ($scope.types[j].id == s[i].typeID) {
                                $scope.types[j].classificationOriginID = s[i].id;
                            }
                        }
                    }
                });
            });

        });
    }

    load();

    $scope.setDefault = function(type) {
        Websites.setDefaultClassificationOrigin(type.classificationOriginID).then(function(r) {
            load();
        });
    };
};