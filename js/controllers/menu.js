'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.Cmenu', []);




//app.controller('menu', ['$scope', '$route', '$rootScope', '$routeParams', '$window', 'Websites', 'UrlNames', 'Preview','WidgetStyles', 'Spiders', 'Types', 'Sources'
//    ,function($scope, $route, $rootScope, $routeParams, $window, Websites, UrlNames, Preview, WidgetStyles, Spiders, Types, Sources){

controllers.menu = function($scope, $route, $rootScope, $routeParams, $window, Websites, UrlNames, Preview, WidgetStyles, Spiders, Types, Sources) {

    $rootScope.testAdmin = false;

    $scope.currentMenu = null;
    $scope.currentCMenu = null;
    $scope.currentUMenu = null;
    $scope.currentWMenu = null;
    $scope.currentLMenu = null;
    $scope.currentDMenu = null;

    $scope.currentWebsiteID = null;
    $scope.currentUrlNameID = null;
    $scope.currentSpiderID = null;
    $scope.currentTypeID = null;

    $scope.spiderUrlBuilderStatus = 0;
    $scope.spiderImportStatus = 0;
    $scope.spiderSearchRegexStatus = 0;
    $scope.spiderJobRegexStatus = 0;

    if ($scope.currentSpiderID != null && $scope.currentSpiderID != 0) {
        $scope.loadSpiderStatus();
    }

    $scope.loadSpiderStatus = function() {
        if ($scope.currentSpiderID != null && $scope.currentSpiderID != 0) {
            Spiders.get($scope.currentSpiderID).then(function(s) {
                console.debug(s);
                $scope.spiderUrlBuilderStatus = s.urlBuilderStatus;
                $scope.spiderImportStatus = s.importStatus;
                $scope.spiderSearchRegexStatus = s.regexSearchStatus;
                $scope.spiderJobRegexStatus = s.regexJobStatus;
            });
        }
    };
    
    $scope.generateSite = function(){
        Websites.generateSite($scope.currentWebsiteID).then(function(data){
            console.debug(data);
        });
    }
    
    $scope.cleanMatchingTree = function(){
        $rootScope.$broadcast('cleanMatchingTree');
    };

    $scope.$on('setCurrent', function(event, menu) {
        $scope.setCurrent(menu);
    });
    $scope.$on('setCurrentC', function(event, menu) {
        $scope.setCurrentC(menu);
    });
    $scope.$on('setCurrentW', function(event, menu) {
        $scope.setCurrentW(menu);
    });
    $scope.$on('setCurrentU', function(event, menu) {
        $scope.setCurrentU(menu);
    });
    $scope.$on('setCurrentL', function(event, menu) {
        $scope.setCurrentL(menu);
    });
    $scope.$on('setCurrentD', function(event, menu) {
        $scope.setCurrentD(menu);
    });

    $scope.$on('loadSpiderStatus', function(event) {
        $scope.loadSpiderStatus();
    });

    $scope.$on('setCurrentWebsiteID', function(event, websiteID) {
        $scope.setCurrentWebsiteID(websiteID);
    });
    $scope.$on('setCurrentUrlNameID', function(event, urlNameID) {
        $scope.setCurrentUrlNameID(urlNameID);
    });
    $scope.$on('setCurrentSpiderID', function(event, spiderID) {
        $scope.setCurrentSpiderID(spiderID);
    });

    $scope.$on('setCurrentTypeID', function(event, typeID) {
        $scope.setCurrentTypeID(typeID);
    });
    $scope.$on('setCurrentSourceID', function(event, sourceID) {
        $scope.setCurrentSourceID(sourceID);
    });
    $scope.$on('spiderCreated', function(event, spiderID) {
        $scope.spiderCreated(spiderID);
    });

    $scope.$on('newWebsiteID', function(event, websiteID) {

        $scope.currentWebsiteID = websiteID;
        load();
        $scope.setCurrentWebsiteID(websiteID);

    });

    $scope.preview = function() {
        if ($scope.currentWebsiteID !== null) {
            Preview.openPopup("http://dev" + $scope.currentWebsiteID + "/allJobs");
        }
        else {
            throw "WebsiteID is null";
        }
    };

    $scope.setCurrentWebsiteID = function(websiteID) {
        $scope.currentWebsiteID = websiteID;
        $("#websiteSelect").val(websiteID);
        $("#websiteSelect").select2('val', websiteID);
        $scope.redirectWebsite();
    };
    $scope.setCurrentUrlNameID = function(urlNameID) {
        $scope.currentUrlNameID = urlNameID;
        $("#urlNameSelect").val(urlNameID);
        $("#urlNameSelect").select2('val', urlNameID);
        $scope.redirectWebsite();
    };
    $scope.setCurrentTypeID = function(typeID) {
        $scope.currentTypeID = typeID;
        $("#typeSelect").val(typeID);
        $("#typeSelect").select2('val', typeID);
        //$scope.redirectWebsite();
    };
    $scope.setCurrentSourceID = function(sourceID) {
        $scope.currentSourceID = sourceID;
        $("#sourceSelect").val(sourceID);
        $("#sourceSelect").select2('val', sourceID);
        //$scope.redirectWebsite();
    };

    $scope.setCurrentSpiderID = function(spiderID) {
        $scope.currentSpiderID = spiderID;
        $("#spiderSelect").val(spiderID);
        $("#spiderSelect").select2('val', spiderID);
        $scope.loadSpiderStatus();

    };

    $scope.setCurrent = function(menu) {
        $scope.currentMenu = menu;
    };
    $scope.setCurrentC = function(menu) {
        $scope.currentCMenu = menu;
    };
    $scope.setCurrentW = function(menu) {
        $scope.currentWMenu = menu;
    };
    $scope.setCurrentU = function(menu) {
        $scope.currentUMenu = menu;
    };
    $scope.setCurrentL = function(menu) {
        $scope.currentLMenu = menu;
    };
    $scope.setCurrentD = function(menu) {
        $scope.currentDMenu = menu;
    };

    $scope.back = function() {
        var url = "/#/managerUrl/" + $scope.currentWebsiteID;
        $window.location.href = url;
    };

    $scope.newWebsite = function() {
        $scope.currentwebsiteID = 0;

        $scope.setCurrentW('general');
        $("#websiteSelect").val(0);
        $window.location.href = "/#/managerWebsite/0";
    };


    $scope.newSpider = function() {

        $scope.setCurrentSpiderID(0);

        $window.location.href = "/#/managerNewSpider";
    };
    $scope.spiderCreated = function(id) {
        load();
        $scope.setCurrentSpiderID(id);
        $window.location.href = "/#/managerSpider/" + spiderID;
    };

    $scope.redirectLayout = function() {
        switch ($scope.currentDMenu) {
            case 'layout' :
                var url = "/#/managerLayout/" + $scope.currentWebsiteID + "/" + $scope.currentUrlNameID;
                break;
            case 'widgetSCSS' :
                var url = "/#/managerWidgetSCSS/" + $scope.currentWebsiteID + "/" + $scope.currentUrlNameID;
                break;
            case 'widgetPosition' :
                var url = "/#/managerWidgetPosition/" + $scope.currentWebsiteID + "/" + $scope.currentUrlNameID;
                break;

            default :
                var url = "/#/managerLayout/" + $scope.currentWebsiteID + "/" + $scope.currentUrlNameID;
                break;
        }


        $window.location.href = url;
    };

    $scope.redirectWebsite = function() {
        switch ($scope.currentWMenu) {
            case 'general' :
                var url = "/#/managerWebsite";
                break;
            case 'seo' :
                var url = "/#/managerSEO";
                break;
            case 'classification' :
                var url = "/#/managerWebsiteClassifications";
                break;
            case 'dev' :
                var url = "/#/managerDEV";
                break;
            case 'urls' :
                var url = "/#/managerUrl";
                break;
            case 'htaccess' :
                var url = "/#/managerHtAccess";
                break;
            case 'design' :
                return;
                break;
            default :
                var url = "/#/managerWebsite";
                break;
        }
        url += "/";
        url += $scope.currentWebsiteID;
        $window.location.href = url;
    };

    $scope.loadSources = function(typeID) {
        Sources.getAll().then(function(s) {
            var sources = [];
            for (var i in s) {
                sources.push({id: s[i].id, text: s[i].name});
            }

            $("#sourceSelect").select2({'width': '400px', data: sources});

            $("#sourceSelect").change(function(event) {
                var id = $("#sourceSelect").val();
                $scope.currentSourceID = id;
                $scope.$apply();
            });
        });
    }
    function load() {

        Spiders.getAll().then(function(s) {
            $scope.spiders = [];
            $scope.spiders.push({id: 0, text: "spider Creation"});
            for (var i in s) {
                $scope.spiders.push({id: s[i].id, text: s[i].auname});
            }

            $("#spiderSelect").select2({'width': '600px', data: $scope.spiders});

            $("#spiderSelect").change(function(event) {
                var id = $("#spiderSelect").val();
                $scope.currentSpiderID = id;
                $scope.$apply();
                if (id == 0) {
                    $scope.newSpider();
                }
                else {
                    //$scope.redirectSpider();
                }
            });
        });

        Types.getAll().then(function(t) {
            var types = [];
            for (var i in t) {
                types.push({id: t[i].id, text: t[i].name});
            }

            $("#typeSelect").select2({'width': '400px', data: types});

            $("#typeSelect").change(function(event) {
                var id = $("#typeSelect").val();
                $scope.currentTypeID = id;
                $scope.$apply();
                $scope.loadSources();
            });
        });

        var promise1 = Websites.getAll();
        promise1.then(function(w) {

            $scope.websites = [];
            $scope.websites.push({id: 0, text: "website Creation"});
            for (var i in w) {
                $scope.websites.push({id: w[i].id, text: w[i].websiteName + " - " + w[i].url});
            }

            $("#websiteSelect").select2({'width': '600px', data: $scope.websites});



            $("#websiteSelect").change(function(event) {
                var id = $("#websiteSelect").val();
                $scope.currentWebsiteID = id;
                $scope.$apply();
                if (id == 0) {
                    $scope.newWebsite();
                }
                else {
                    $scope.redirectWebsite();
                }
            });

        }, function(reason) {
            console.debug(reason);
        });

        UrlNames.getAll().then(function(d) {
            $scope.urlNames = [];
            for (var i in d) {
                $scope.urlNames.push({id: d[i].id, text: d[i].urlName});
            }

            $("#urlNameSelect").select2({'width': '600px', data: $scope.urlNames});



            $("#urlNameSelect").change(function(event) {
                var id = $("#urlNameSelect").val();
                $scope.currentUrlNameID = id;
                $scope.$apply();
                $scope.redirectLayout();
            });
        });
    }
    load();

    $scope.reload = function() {
        WidgetStyles.reload($scope.currentWebsiteID).then(function() {
            $route.reload();
        });
    };




};
