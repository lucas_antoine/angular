'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.IDEwidgetPosition', []);

//alias of seoPageData
//app.controller('widgetPosition', ['$scope', '$rootScope', '$routeParams', '$compile', 'Layouts', 'Widgets', 'Options', 'WidgetContainers','Preview'
//    , function($scope, $rootScope, $routeParams, $compile, Layouts, Widgets, Options, WidgetContainers,Preview) {
controllers.widgetPosition = function($scope, $rootScope, $routeParams, $modal, $compile, Layouts, Widgets, Options, WidgetContainers, Preview, ScssStore, ScssWidgetStoreHelper, WidgetStyles) {
    $rootScope.$broadcast('setCurrent', 'websites');
    $rootScope.$broadcast('setCurrentW', 'design');
    $rootScope.$broadcast('setCurrentD', 'widgetPosition');
    $rootScope.$broadcast('setCurrentWebsiteID', $routeParams.websiteID);
    $rootScope.$broadcast('setCurrentUrlNameID', $routeParams.urlNameID);


    $scope.tinymceModel = "<b>Hello World</b>";

    $scope.testAdmin = $rootScope.testAdmin;
    $scope.processing = false;
    $scope.websiteID = $routeParams.websiteID;
    $scope.urlNameID = $routeParams.urlNameID;

    $scope.primarycolorchoices = [];


    ScssWidgetStoreHelper.loadPrimaryColors($scope.websiteID).then(function(c) {
        for (var i in c) {
            $scope.primarycolorchoices.push(ScssColor.new(c[i], $scope.websiteID, null, [WidgetStyles, Preview]));
        }

    });

    /**
     * Tiny MCE
     */

    $scope.value = "<p>Hello World !</p>";

    $scope.config = {
        beforeRender: function(ed) {
            ScssStore.loadPrimaryColors($scope.websiteID).then(function(c) {
                var textColorMap = [];
                $scope.pc = [];
                for (var i in c) {
                    if (c[i].widgetStyless.length != 0) {
                        var value = c[i]['widgetStyless']['_0'].variableValue;
                    }
                    else {
                        var value = c[i].defaultValue;
                    }
                    var name = c[i].variableName;
                    $scope.pc.push({name: name, value: value});
                    textColorMap.push(value.slice(1));
                    textColorMap.push(name);
                }
                ed.settings.textcolor_map = textColorMap;

                //ed.save();

            });
        },
        toolbar: "forecolor backcolor \n\
      \n\styleselect  fontselect fontsizeselect bullist numlist outdent indent blockquote image",
        //menubar: true,
        menubar: "paste table format hr insertdatetime ",
        plugins: "textcolor code table paste insertdatetime image",
        textcolor_rows: 2,
        textcolor_cols: 5,
        theme_advanced_resizing: true,
        theme_advanced_resizing_use_cookie: false
    };

    /*$scope.changeText = function(){
     $timeout(function(){
     console.debug("click on changeText");
     $scope.value = "<p>Hello Man</p>";
     });
     };
     */
    $scope.saveWYSIWYG = function() {
        currentOption.value = $scope.value;
        $scope.updateOption(currentOption, currentWidget);
        $("#tinymcecontainer").hide();
    };

    $scope.cancelWYSIWYG = function() {
        $("#tinymcecontainer").hide();
    };


    var currentWidget = null
    var currentOption = null;

    $scope.tinymce = function(widget, option) {
        currentWidget = widget;
        currentOption = option;

        var width = widget.parent.width + "px";
        $("#tinymce").css("width", width);
        $("#tinymce").css("min-height", "300px");
        $("#tinymcecontainer").show();

        $scope.value = option.value;
    };

    /**
     * End TinyMCE
     */

    Widgets.getAll($scope.urlNameID).then(function(w) {
        $scope.widgets = w;
    });

    $scope.showCSS = function(widgetID) {
        $(".widgetPositionOption").hide();
        $(".widgetPositionCSS").hide();
        $("#C_" + widgetID).show();
    }

    $scope.showOptions = function(widgetID) {
        $(".widgetPositionOption").hide();
        $(".widgetPositionCSS").hide();
        $("#O_" + widgetID).show();
    }

    $scope.updateWidget = function(widget, reload) {

        WidgetContainers.updateTheme(widget.getObject(), $scope.websiteID).then(function() {

            if (reload) {
                load();
            }
        });
    };

    $scope.widgetMenu = function(container, $event) {
        $scope.currentContainer = container;
        $("#context1").css({
            position: "absolute",
            left: $event.pageX + 'px',
            top: $event.pageY + 'px'
        }).show();
    };

    $scope.move = function(containerWidgetID, action) {
        $scope.processing = true;
        WidgetContainers.updateOrder($scope.websiteID, containerWidgetID, action).then(function() {
            load();
            $scope.processing = false;
        });
    };



    $scope.addWidget = function(widget) {

        $("#context1").hide();
        $scope.processing = true;

        var containerWidget = {
            containerID: $scope.currentContainer.id,
            widgetID: widget.id,
            position: $scope.currentContainer.widgets.length + 1
        };

        WidgetContainers.add(containerWidget, $scope.websiteID).then(function(containerWidget) {
            var wi = $scope.currentContainer.addWidget(containerWidget);
            for (var m in $scope.widgets) {
                if ($scope.widgets[m].id == wi.widgetID) {
                    wi.setThemes($scope.widgets[m].widgetThemess);
                }
            }
            //getting options
            WidgetContainers.getOptions(wi.themeID, wi.id).then(function(r) {
                wi.setOptions(r);
                $scope.processing = false;
                load();
            });
        });
    };

    $scope.removeWidget = function(widget) {
        $scope.processing = true;
        Preview.setRefreshStatus(true);
        WidgetContainers.remove(widget.id, $scope.websiteID).then(function() {
            widget.remove();
            Preview.setRefreshStatus(true);
            $scope.processing = false;
        });
    };

    $scope.updateOption = function(option, widget) {
        Preview.setRefreshStatus(true);
        $scope.processing = true;
        WidgetContainers.updateOptions(option, widget, $scope.websiteID).then(function(id) {
            option.containerWidgetOptionID = id;
            $scope.processing = false;
            Preview.setRefreshStatus(false);
        });
    };


    $(document).tooltip({
        items: "[data-geo]",
        tooltipClass: "mytooltip",
        content: function() {
            var element = $(this);
            if (element.is("[data-geo]")) {

                var uri = $(this).attr("data-geo");
                return "<img class='map' src='" + uri + "' />";
            }
        }
    });


    Options.get($scope.websiteID).then(function(o) {
        $scope.options = o;
        load();
    });
    function load() {

        $scope.layout = new layoutP($scope.options.width, $scope.options.nbColumn, $scope.options.gutter, $scope);

        Layouts.get($scope.websiteID, $scope.urlNameID).then(function(l) {
            $scope.layout.id = l.id;

            Layouts.getRow0(l.id).then(function(r0) {
                for (var i in r0) {
                    var row = $scope.layout.addRow(r0[i]);
                    (function(row) {
                        Layouts.getContainers(row.id).then(function(c0) {

                            for (var j in c0) {
                                var cont = row.addContainer(c0[j]);
                                (function(cont) {
                                    Layouts.getRows(cont.id).then(function(r1) {
                                        if (r1.length > 0) {
                                            for (var k in r1) {
                                                var row2 = cont.addRow(r1[k]);

                                                (function(row2) {

                                                    Layouts.getContainers(row2.id).then(function(c1) {

                                                        for (var l in c1) {
                                                            var cont2 = row2.addContainer(c1[l]);
                                                            //Get Widgets
                                                            WidgetContainers.getAll(cont2.id).then(function(w) {
                                                                for (var l in w) {
                                                                    var widget = cont2.addWidget(w[l]);
                                                                    for (var m in $scope.widgets) {
                                                                        if ($scope.widgets[m].id == widget.widgetID) {
                                                                            widget.setThemes($scope.widgets[m].widgetThemess);
                                                                            //console.debug($scope.layout);
                                                                        }
                                                                    }
                                                                }
                                                                //loadSortable();
                                                            });
                                                        }
                                                    });
                                                })(row2);
                                            }
                                        }
                                        else {
                                            //get Widgets
                                            WidgetContainers.getAll(cont.id).then(function(w) {
                                                for (var l in w) {
                                                    var widget = cont.addWidget(w[l]);
                                                    for (var m in $scope.widgets) {
                                                        if ($scope.widgets[m].id == widget.widgetID) {
                                                            widget.setThemes($scope.widgets[m].widgetThemess);

                                                        }
                                                    }
                                                    //getting options
                                                    (function(widget) {
                                                        WidgetContainers.getOptions(widget.themeID, widget.id).then(function(r) {
                                                            widget.setOptions(r);

                                                        });
                                                    })(widget);

                                                }
                                                //loadSortable();
                                            });
                                        }
                                    });
                                })(cont);
                            }
                        });
                    })(row);
                }
            });
        });
    }

    function getContainer(containerID) {
        for (var i in $scope.layout.rows) {
            for (var j in $scope.layout.rows[i].containers) {
                if ($scope.layout.rows[i].containers[j].id == containerID) {
                    return $scope.layout.rows[i].containers[j];
                }
                if ($scope.layout.rows[i].containers[j].rows.length > 0) {
                    for (var k in $scope.layout.rows[i].containers[j].rows) {
                        for (var l in $scope.layout.rows[i].containers[j].rows[k].containers) {
                            if ($scope.layout.rows[i].containers[j].rows[k].containers[l].id == containerID) {
                                return $scope.layout.rows[i].containers[j].rows[k].containers[l];
                            }
                        }
                    }
                }
            }
        }
        throw "Can't find containerID " + containerID;
    }


};