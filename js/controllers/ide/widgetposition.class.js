/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
var layoutP = function(width, nbColumns, gutter, scope) {
    this.scope = scope;
    this.gutter = gutter;
    this.nbColumn = nbColumns;
    this.width = width;
    this.id = null;
    this.rows = [];

    this.addRow = function(RowObject) {
        var that = this;
        this.rows.push(new rowP(RowObject, that));
        return this.rows[this.rows.length - 1];
    };
};

var rowP = function(RowObject, Layout) {
    this.layout = Layout;
    this.containers = [];
    this.id = RowObject.id;
    this.nbColumn = RowObject.columns;

    this.addContainer = function(ContainerObject) {
        this.containers.push(new containerP(ContainerObject, this.containers.length, this.layout));
        return this.containers[this.containers.length - 1];
    };


};

var containerP = function(ContainerObject, position, Layout) {

    this.rows = [];
    this.widgets = [];
    this.position = position;
    this.layout = Layout;
    this.gutter = this.layout.gutter;
    this.nbColumn = ContainerObject.columns;
    this.id = ContainerObject.id;
    this.marginLeft = this.position == 0 ? 0 : this.gutter;
    //var margin = parseInt($scope.margin, 10);
    var width = this.layout.width + this.gutter;
    this.width = Math.floor((this.nbColumn * (width / this.layout.nbColumn)) - this.gutter);




    this.addWidget = function(WidgetObject) {
        var that = this;
        console.debug(WidgetObject);
        this.widgets.push(new widgetP(WidgetObject, that, this.layout));
        return this.widgets[this.widgets.length - 1];
    };

    this.removeWidget = function(widgetID) {
        for (var i in this.widgets) {
            if (this.widgets[i].id == widgetID) {
                this.widgets.splice(i, 1);
            }
        }
    }

    this.addRow = function(RowObject) {
        this.rows.push(new rowP(RowObject, this.layout));
        return this.rows[this.rows.length - 1];
    };
};

var widgetP = function(WidgetObject, Container, Layout) {
    console.debug("widgetObject : ");
    console.debug(WidgetObject);
    this.layout = Layout;
    this.id = WidgetObject.id;
    this.widgetID = WidgetObject.widgetThemes.widgetID;
    this.widgetThemeID = WidgetObject.widgetThemes.id;
    this.widgetName = WidgetObject.widgetThemes.widgets.name;
    this.themeID = WidgetObject.widgetThemeID;
    this.position = WidgetObject.position;

    this.backgroundRepeat = WidgetObject.backgroundRepeat;
    this.backgroundPosition = WidgetObject.backgroundPosition;
    this.backgroundColor = WidgetObject.backgroundColor;

    //todo backgroundImage


    this.marginTop = WidgetObject.marginTop;
    this.marginLeft = WidgetObject.marginLeft;
    this.marginRight = WidgetObject.marginRight;
    this.marginBottom = WidgetObject.marginBottom;

    this.paddingTop = WidgetObject.paddingTop;
    this.paddingLeft = WidgetObject.paddingLeft;
    this.paddingRight = WidgetObject.paddingRight;
    this.paddingBottom = WidgetObject.paddingBottom;

    this.borderTopSize = WidgetObject.borderTopSize;
    this.borderLeftSize = WidgetObject.borderLeftSize;
    this.borderRightSize = WidgetObject.borderRightSize;
    this.borderBottomSize = WidgetObject.borderBottomSize;

    this.borderTopStyle = WidgetObject.borderTopStyle;
    this.borderLeftStyle = WidgetObject.borderLeftStyle;
    this.borderRightStyle = WidgetObject.borderRightStyle;
    this.borderBottomStyle = WidgetObject.borderBottomStyle;

    this.borderTopColor = WidgetObject.borderTopColor;
    this.borderLeftColor = WidgetObject.borderLeftColor;
    this.borderRightColor = WidgetObject.borderRightColor;
    this.borderBottomColor = WidgetObject.borderBottomColor;

    this.borderTopLeftRadius = WidgetObject.borderTopLeftRadius;
    this.borderTopRightRadius = WidgetObject.borderTopRightRadius;
    this.borderBottomRightRadius = WidgetObject.borderBottomRightRadius;
    this.borderBottomLeftRadius = WidgetObject.borderBottomLeftRadius;


    this.align = WidgetObject.align;
    this.options = [];
    this.parent = Container;
    this.description = WidgetObject.widgetThemes.widgets.description;
    this.themeList = [];

    this.getObject = function() {
        return {
            id : this.id,
            marginTop: this.marginTop,
            marginLeft: this.marginLeft,
            marginRight: this.marginRight,
            marginBottom: this.marginBottom,
            paddingTop: this.paddingTop,
            paddingLeft: this.paddingLeft,
            paddingRight: this.paddingRight,
            paddingBottom: this.paddingBottom,
            borderTopSize: this.borderTopSize,
            borderLeftSize: this.borderLeftSize,
            borderRightSize: this.borderRightSize,
            borderBottomSize: this.borderBottomSize,
            borderTopStyle: this.borderTopStyle,
            borderLeftStyle: this.borderLeftStyle,
            borderRightStyle: this.borderRightStyle,
            borderBottomStyle: this.borderBottomStyle,
            borderTopColor: this.borderTopColor,
            borderLeftColor: this.borderLeftColor,
            borderRightColor: this.borderRightColor,
            borderBottomColor: this.borderBottomColor,
            borderTopLeftRadius: this.borderTopLeftRadius,
            borderTopRightRadius: this.borderTopRightRadius,
            borderBottomRightRadius: this.borderBottomRightRadius,
            borderBottomLeftRadius: this.borderBottomLeftRadius,
            position: this.position,
            backgroundRepeat: this.backgroundRepeat,
            backgroundPosition: this.backgroundPosition,
            backgroundColor: this.backgroundColor,
            align: this.align,
            widgetThemeID: this.widgetThemeID
        };
    };

    this.setThemes = function(list) {
        this.themeList = list;
    };

    this.setOptions = function(optionList) {
        this.options = optionList;
        for (var i in this.options) {
            if (this.options[i].value == null && this.options[i].defaultValue != null) {
                if (this.options[i].type == "css" && this.options[i].allowedValues == null) {
                    this.options[i].value = parseFloat(this.options[i].defaultValue);
                }
                else {
                    this.options[i].value = this.options[i].defaultValue;
                }
            }
            else {
                if (this.options[i].type == "css" && this.options[i].allowedValues == null) {
                    this.options[i].value = parseFloat(this.options[i].value);
                }
            }
        }
    };

    this.save = function(option) {
        console.debug("sav called");
        console.debug(option);
    }




    this.remove = function() {
        this.parent.removeWidget(this.id);
    };
};