/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
/**
 * 
 * @param {string} width
 * @param {string} gutter
 * @returns {_L6.Document}
 */
var Document = Base.extend({
    initialize: function(width, gutter, columns, scope) {
        
        this.scope = scope;
        
        this.widgetMenu = true;
        /**
         * 
         * @type int
         */
        this.width = parseInt(width, 10);
        /**
         * 
         * @type int
         */
        this.gutter = parseInt(gutter, 10);

        this.columns = columns;

        /**
         * 
         * @type DoublyLinkedList
         */
        this.rows = DoubleLinkedList.new ();
    },
    /**
     * 
     * @returns {DoubleLinkedNode}
     */
    addRow: function() {

        return this.rows.insertEnd(Row.new (this.columns, this));
    },
    /**
     * 
     * @param {Row} Row
     * @returns {Boolean}
     */
    removeRow: function(Row) {
        return this.rows.removeObject(Row);
    },
    generateHTML: function(withID) {

        var HTML = "<div style='margin:auto;width:" + (this.width + 8) + "px;min-height:50px;height:auto' >\n";
        var rowNode = this.rows.firstNode;

        while (rowNode !== null) {
            HTML += rowNode.generateHTML(withID);
            rowNode = rowNode.next;
        }
        HTML += "</div>\n";
        return HTML;
    },
    serialize : function(){
        var rows = [];
        var r = this.rows.firstNode;
        
        while(r!==null){
            rows.push(r.serialize());
            r = r.next;
        }
        return rows;
    }
});

/**
 * this is a column. A column can have rows inside
 * 
 * @param {int} position
 * @param {int} nbColumn
 * @param {Row} parentRow
 * @returns {_L6.Container}
 */
var Container = DoubleLinkedNode.extend({
    initialize: function(nbColumn, doc, parentRow) {
        
        this.containerID = null;
        
        /**
         * 
         * @type int
         */
        this.nbColumn = nbColumn;


        /**
         * 
         * @type int
         */
        this.exactWidth = null;

        /**
         * 
         * @type Row
         */
        this.parentRow = parentRow;
        /**
         * 
         * @type DoubleLinkedList
         */
        this.rows = DoubleLinkedList.new ();


        this.document = doc;

        this.minHeight = "75px";
        
        this.widgets = [];
    },
            
    addWidget: function(widget){
        this.widgets.push(widget);
    }, 
    setWidgetTheme : function(widgetID,themeID){
        var w = this.getWidget(widgetID);
        w.widgetThemeID = themeID;
        w.validate();
    },
    getWidget : function(widgetID){
        for(var i in this.widgets){
            if(this.widgets[i].widget.id == widgetID){
                return this.widgets[i].widget;
            }
        }
        throw "Widget not found. Id = "+widgetID;
    },
    removeWidget: function(widgetID){
        for(var i in this.widgets){
            if(this.widgets[i].widget.id == widgetID){
                this.widgets.splice(i,1);
                return true;
            }
        }
        return false;
    },
    addRow: function() {
        ///we dont allow mo than 2 level ebsting here
        if (this.parentRow.parentContainer != null) {
            this.parentRow.parentContainer.addRow();
        }
        else {

            if (this.rows.length === 0) {
                this.rows.insertEnd(Row.new (this.nbColumn, this.document, this));
                this.rows.insertEnd(Row.new (this.nbColumn, this.document, this));
            }
            else {
                this.rows.insertEnd(Row.new (this.nbColumn, this.document, this));
            }
        }
    },
    //By default, try to modify the with of the element just on its right. Take the one on the left only if it is the last element, or if the next element has only 1 column wide.  
    incrWidth: function() {
        if (this.parentRow.nbColumn === this.nbColumn) {
            return false;
        }
        var isLast = (this.parentRow.containers.length - 1) === this.index ? true : false;
        var isFirst = this.index === 0 ? true : false;

        if (isLast && isFirst) {
            if(this.parentRow.nbColumn != this.nbColumn){
                this.nbColumn = this.parentRow.nbColumn;
            }
            return false;
        }

        //is the last
        if (isLast) {
            //no space left
            if (this.prev.nbColumn === 1) {
                return false;
            }
            else{
                
                
                this.prev.nbColumn--;
                this.nbColumn++;
                //check if the previous container has rows
                if(this.prev.rows.length>0 && this.rows.length===0){
                    console.debug("CASE A");
                    var row = this.prev.rows.firstNode;
                    while(row!==null){
                        row.nbColumn--;
                        row.detectColumnChange();
                        row = row.next;
                    }
                }
                
                return true;
            }
        }
        //is not the last
        else {
            if (this.next.nbColumn === 1) {
                if (!isFirst) {
                    if (this.prev.nbColumn === 1) {
                        return false;
                    }
                    else{
                        this.prev.nbColumn--;
                        this.nbColumn++;
                        //check if the previous container has rows
                        if(this.prev.rows.length>0 && this.rows.length===0){
                            var row = this.prev.rows.firstNode;
                            while(row!==null){
                                row.nbColumn--;
                                row.detectColumnChange();
                                row = row.next;
                            }
                        }
                        
                        return true;
                    }
                }
                else {
                    return false;
                }
            }
            
            
            this.next.nbColumn--;
            this.nbColumn++;
            
            //check if the next container has rows
            if(this.next.rows.length>0 && this.rows.length===0){
                console.debug("CASE C");
                var row = this.next.rows.firstNode;
                while(row!==null){
                    row.nbColumn--;
                    row.detectColumnChange();
                    row = row.next;
                }
            }
            
            return true;
        }
    },
    decrWidth: function() {
        if (this.parentRow.nbColumn === this.nbColumn) {
            return false;
        }
        if (this.nbColumn === 1) {
            return false;
        }

        var isLast = (this.parentRow.containers.length - 1) === this.index ? true : false;
        var isFirst = this.index === 0 ? true : false;


        if (isLast && isFirst) {
            if(this.parentRow.nbColumn != this.nbColumn){
                this.nbColumn = this.parentRow.nbColumn;
            }
            return false;
        }

        //is the last
        if (isLast) {
            //check if the previous container has rows
            if(this.prev.rows.length>0 && this.rows.length==0){
                var row = this.prev.rows.firstNode;
                while(row!==null){
                    row.nbColumn++;
                    row.detectColumnChange();
                    row = row.next;
                }
            }
            this.prev.nbColumn++;
        }
        //is not the last
        else {
            //check if the next container has rows
            if(this.next.rows.length>0 && this.rows.length==0){
                var row = this.next.rows.firstNode;
                while(row!==null){
                    row.nbColumn++;
                    row.detectColumnChange();
                    row = row.next;
                }
            }
            this.next.nbColumn++;
        }
        this.nbColumn--;
        return true;
    },
    calculateExactWidth: function() {
        //var margin = parseInt($scope.margin, 10);
        var gutter = this.document.gutter;
        var width = this.document.width + gutter;
        this.exactWidth = Math.floor((this.nbColumn * (width / this.document.columns)) - gutter);

        var l = 0;
        for (var i = 0; i < this.parentRow.containers.length; i++) {
            var elem = this.parentRow.containers.getElementAt(i);
            if (elem.rows.length > l) {
                l = elem.rows.length;
            }
        }
        this.minHeight = l == 0 ? "90px" : (90 * l + this.document.gutter * (l - 1)) + "px";


    },
    generateID: function() {
        this.parentRow.generateID();
        this.id = this.parentRow.id + "_C" + this.index;
    },
    generateHTML: function(withID) {
        this.generateID();
        this.calculateExactWidth();
        var widgetMenu =this.document.widgetMenu;

        var style = "";
        var cvar1 = 130;
        var cvar2 = 139;
        var new_light_color = 'rgb(' + (Math.floor((256 - cvar2) * Math.random()) + cvar1) + ',' +
                (Math.floor((256 - cvar2) * Math.random()) + cvar1) + ',' +
                (Math.floor((256 - cvar2) * Math.random()) + cvar1) + ')';
        
        
        if (this.index !== 0) {
            style = " activecontainer='activecontainer' style='float:left;min-height:" + this.minHeight + ";height:auto;margin-left:" + this.document.gutter + "px;margin-bottom:" + this.document.gutter + "px;";
        }
        else {
            style = " activecontainer='activecontainer' style='float:left;min-height:" + this.minHeight + ";height:auto;margin-bottom:" + this.document.gutter + "px;";
        }
        if (withID === true) {
            if (this.rows.length > 0) {
                var id = " id='" + this.id + "' class='su_filled_container' ";
            }
            else {
                var id = " id='" + this.id + "' class='su_container' ";
            }
            style += "width:" + this.exactWidth + "px;background-color: " + new_light_color + ";' ";
        }
        else {
            var id = "";
            style += "width:" + this.exactWidth + "px;' ";
        }
        
        
        if (this.rows.length > 0) {
            var HTML = "<div " + id + style + " >\n";
            
            var rowNode = this.rows.firstNode;
            while (rowNode !== null) {
                HTML += rowNode.generateHTML(withID);
                rowNode = rowNode.next;
            }
            
            HTML += "</div>\n";
        }
        else{
            if(!this.document.widgetMenu){
                var HTML = "<div  ng-click='editContainer($event)' " + id + style + " >\n";                    
                HTML += "</div>\n";
                
            }
            else{
                var HTML = "<div " + id + style + " >\n";
                    HTML+="<div style='padding:0px;margin:0px;' ng-show='"+this.document.widgetMenu+"'  >";
                        HTML+="<button ng-click='widgetMenu($event)' type='button'><i class='icon-plus'></i></button>";
                    HTML+="</div>";
                    HTML +="<ul ui-sortable class='sort'>"
                    for(var i in this.widgets){
                        HTML += this.widgets[i].generateHTML();
                    }
                    HTML += "</ul>";
                HTML += "</div>\n";
            }
        }
        
        
        return HTML;
    },
    serialize: function(){
        var data = {};
        data.nbColumn = this.nbColumn;
        data.rows = [];
        data.containerID = this.containerID;
        var r = this.rows.firstNode;
        while(r!=null){
            data.rows.push(r.serialize());
            r = r.next;
        }
        return data;
    }
    
});

/**
 * This is a row. A row can have many columns (containers). At Construction, one Large Container is created 
 * 
 * @param {int} nbColumn
 * @returns {_L6.Row}
 */
var Row = DoubleLinkedNode.extend({
    initialize: function(nbColumn, doc, parentContainer) {
        
        this.rowID = null;
        /**
         * 
         * @type int
         */
        this.nbColumn = nbColumn;

        this.document = doc;

        /**
         * 
         * @type Container|null
         */
        this.parentContainer = parentContainer;
        /**
         * @type Integer
         */
        this.previousColumns =this.nbColumn;
        /**
         * 
         * @type DoubleLinkedList
         */
        this.containers = DoubleLinkedList.new ();
        this.addContainer();
    },
    detectColumnChange : function(){
        if(this.previousColumns ===null){
            throw "previsou column not set";
        }
        else{
            if(this.previousColumns != this.nbColumn){
                //change happened
                if(this.parentContainer!=null){
                    //I'm concerned with this change
                    var success = false;
                    var lastContainer = this.containers.lastNode;
                    while(success==false && lastContainer!=null){
                        
                        if(this.previousColumns > this.nbColumn){
                            //decrease
                            if(lastContainer.nbColumn>1){
                                var diff = this.previousColumns - this.nbColumn;
                                lastContainer.nbColumn-= diff;
                                success=true
                            }
                        }
                        else{
                            //increase
                            var diff = this.nbColumn - this.previousColumns;
                            lastContainer.nbColumn+= diff;
                            success=true                            
                        }
                        lastContainer = lastContainer.prev;                      
                    }
                }
                this.previousColumns = this.nbColumn;
            }
        }
    },
    removeContainer: function(index) {
        if (this.containers.length === 1 && this.parentContainer != null) {
            this.parentContainer.rows.removeObjectAt(this.index);
        }
        else if (this.containers.length === 1) {
            this.document.rows.removeObjectAt(this.index);            
        }
        else {
            var toDelete = this.containers.getElementAt(index);
            if (this.containers.length === (index + 1)) {
                //this is the last element
                var container = toDelete.prev;
                container.nbColumn += toDelete.nbColumn;                
            }
            else {
                var container = toDelete.next;
                container.nbColumn += toDelete.nbColumn;
                
            }
            if(container.rows.length>0 && toDelete.rows.length==0){
                var row = container.rows.firstNode;
                console.debug(container.rows);
                while(row!=null){
                    console.debug("increasing width of "+row.id);
                    row.nbColumn += toDelete.nbColumn;
                    try{
                        row.detectColumnChange();
                    }
                    catch(e){
                        console.debug(row);
                        console.debug("Excepotion!");
                    };
                    row = row.next;
                }
            }
            this.containers.removeObjectAt(index);
        }
    },
    addContainer: function() {
        if (this.containers.length === 0) {

            return this.containers.insertEnd(Container.new (this.nbColumn, this.document, this));
        }
        else {
            var firstNode = this.containers.firstNode;
            var newWidth = 0;
            while (firstNode !== null) {
                if (firstNode.nbColumn > 1) {
                    firstNode.nbColumn--;
                    newWidth++;
                }
                firstNode = firstNode.next;
            }
            if (newWidth > 0) {
                return this.containers.insertEnd(Container.new (newWidth, this.document, this));
            }
            else {
                throw "No space left to add a container";
            }
        }
    },
    addContainerAfter: function(container) {

        var firstNode = this.containers.firstNode;
        var newWidth = 0;
        while (firstNode !== null) {
            if (firstNode.nbColumn > 1) {
                firstNode.nbColumn--;
                newWidth++;
            }
            firstNode = firstNode.next;
        }
        if (newWidth > 0) {

            return this.containers.insertAfter(container, Container.new (newWidth, this.document, this));
        }
        else {
            throw "No space left to add a container";
        }
    },
    addContainerBefore: function(container) {


        var firstNode = this.containers.firstNode;
        var newWidth = 0;
        while (firstNode !== null) {
            if (firstNode.nbColumn > 1) {
                firstNode.nbColumn--;
                newWidth++;
            }
            firstNode = firstNode.next;
        }
        if (newWidth > 0) {
            return this.containers.insertBefore(container, Container.new (newWidth, this.document, this));
        }
        else {
            throw "No space left to add a container";
        }
    },
    generateID: function() {
        if (this.parentContainer == null) {
            this.id = "R" + this.index;
        }
        else {
            this.parentContainer.generateID();
            this.id = this.parentContainer.id + "_R" + this.index;
        }
    },
    generateHTML: function(withID) {

        if (this.containers.length === 0) {
            return "";
        }
        this.generateID();
        var style = "";
        if (withID === true) {
            var id = " id='" + this.id + "'  class='su_row' ";

            style = " style='float:left;width:100%;padding:0px;margin:0px;background-color:#fff;' ";
        }
        else {
            var id = "";
            style = " style='float:left;width:100%;padding:0px;margin:0px;' ";
        }

        var HTML = "<div " + id + style + " >\n";

        var containerNode = this.containers.firstNode;
        while (containerNode !== null) {
            HTML += containerNode.generateHTML(withID);
            containerNode = containerNode.next;
        }

        HTML += "</div>\n";
        return HTML;
    },
    serialize: function(){
        var data = {};
        data.containers = [];
        data.nbColumn =  this.nbColumn;
        data.rowID = this.rowID;
        
        var c = this.containers.firstNode;
        while(c != null){
            data.containers.push(c.serialize());
            c = c.next;
        }
        return data;
        
    }

});

var WidgetObject = Base.extend({
    initialize: function(widget, containerID) {
        this.containerID = containerID;
        this.widget = widget;
        this.options = [];
        this.themes = {};
        for(var i in this.widget.widgetOptionss){
            this.options.push(this.widget.widgetOptionss[i]);
        }
        for(var i in this.widget.widgetThemess){
            this.themes[this.widget.widgetThemess[i].themeName] = this.widget.widgetThemess[i];
        }
        this.widgetThemeID = null;
        this.valid = false;
        this.saved = false;
    },
    validate : function(){
        if(this.widgetThemeID == null){
            this.valid = false;
        }
        else{
            this.valid = true;
        }
    },
    populate : function(containerWidgetOptions){
        this.containerWidgetOptions = containerWidgetOptions;
    },
    getOptionValue : function(optionID){
        for(var i in this.containerWidgetOptions ){
            if(this.containerWidgetOptions[i].widgetOptionID == optionID){
                return this.containerWidgetOptions[i].value;
            }
        }
        for(var i in this.options){
            if(this.options[i]==optionID){
                return this.options[i].defaultValue;
            }
        }
    },
    generateHTML : function(){
        var validClass = this.valid ? "valid" : "invalid";
        var HTML = "<li class='widget "+validClass+"'>";
        HTML += "<b>"+this.widget.name+"</b> <button ng-disabled='processing' type='button' ng-click='remove("+this.widget.id+",\""+this.containerID+"\", \""+this.containerID+"\", "+this.widget.id+")'> - </button><br />";
        HTML += "Theme : <select style='width:100px' ng-model='theme_"+this.widget.id+"_"+this.containerID+"' ng-disabled='processing' ng-change='updateWidgetTheme(\"theme_"+this.widget.id+"_"+this.containerID+"\")'>";
        for(var i in this.themes){
            HTML += "<option value='"+this.themes[i].id+"'>"+i+"</option>";
        }
        HTML += "</select><br />";
        
        for(var i in this.options){
            HTML += this.options[i].optionName+" : ";
            
            switch(this.options[i].type){
                case "css" : 
                    if(this.options[i].allowedValues!=null){
                        HTML +="<select  ng-disabled='processing' ng-model='widget_"+this.widget.id+"_"+this.options[i].id+"_"+this.containerID+"' style='width:100px'>";
                        var allowedValues = eval(this.options[i].allowedValues);                        
                        for(var j in allowedValues){                            
                            HTML +="<option value='"+allowedValues[j]+"'>"+allowedValues[j]+"</option>";
                        }
                        HTML+="</select>";
                    }
                    else{
                        HTML += "<input  ng-disabled='processing' type='number' ng-model='widget_"+this.options[i].id+"_"+this.containerID+"' value='"+this.getOptionValue(this.options[i].id)+"' ";
                        if(this.options[i].min!=null){
                            HTML += " min='"+this.options[i].min+"' ";
                        }
                        if(this.options[i].max!=null){
                            HTML += " max='"+this.options[i].max+"' ";
                        }
                        HTML += "> <br />";
                    }
                    break;
                case "boolean" : 
                    HTML += "<input  ng-disabled='processing' type='checkbox' ng-model='widget_"+this.options[i].id+"_"+this.containerID+"' ";
                    var val = this.getOptionValue(this.options[i].id);
                    if(val == true){
                        HTML += " checked='checked' "
                    }
                    HTML += "> <br />";
                    break;
                case "file" : 
                    HTML += "TODO <br />"
                    break;
                default : 
                    throw "Unknown option type : "+this.options[i].type;
            }
        }
        HTML += "</li>";
        return HTML;
    }
    
});