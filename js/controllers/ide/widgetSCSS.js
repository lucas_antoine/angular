'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.IDEwidgetSCSS', []);



//alias of seoPageData
//app.controller('widgetSCSS', ['$scope', '$rootScope', '$routeParams', '$compile', '$q',  'ScssStore', 'Color', 'WidgetThemeVariables', 'WidgetStyles', 'Preview', 'UrlNames', 'ScssWidgetStoreHelper'
//    ,function($scope, $rootScope, $routeParams, $compile, $q,  ScssStore, Color, WidgetThemeVariables, WidgetStyles, Preview, UrlNames, ScssWidgetStoreHelper) {
controllers.widgetSCSS = function($scope, $rootScope, $routeParams, $compile, $q,  ScssStore, Color, WidgetThemeVariables, WidgetStyles, Preview, UrlNames, ScssWidgetStoreHelper) {
    $rootScope.$broadcast('setCurrent', 'websites');
    $rootScope.$broadcast('setCurrentW', 'design');
    $rootScope.$broadcast('setCurrentD', 'widgetSCSS');
    $rootScope.$broadcast('setCurrentWebsiteID', $routeParams.websiteID);

    $scope.testAdmin = $rootScope.testAdmin;

    $scope.processing = false;
    $scope.websiteID = $routeParams.websiteID;

    $scope.currentPage = "search";
    $scope.fontfamilies = ['"Droid Sans", sans-serif','"Arvo", serif','"PT Sans", sans-serif','"Ubuntu", sans-serif','"Josefin Slab", serif','"Open Sans", sans-serif','"Open Sans Condensed", sans-serif','"Vollkorn", serif','"Abril Fatface", cursive','"Old Standard TT", serif','"Advent Pro", sans-serif'];
    $scope.loaded = false;
    $scope.primarycolorchoices = [];

    $scope.pages = {Colors: -1,SiteGeneral : -2, WidgetCommon: -5, Crumb: -3, Label: -4,  Typo: -8, sidenav: -6, table: -7 };

    /*ScssWidgetStoreHelper.loadPrimaryColors($scope.websiteID).then(function(c){
        for(var i in c){
            $scope.primarycolorchoices.push(ScssColor.new(c[i], $scope.websiteID, null, [WidgetStyles, Preview]));
            //ScssWidgetStore.setType(ScssColor.new(c[i],$scope.websiteID, null, [WidgetStyles, Preview]));
        }
    });*/

    $scope.changeUrlID = function(urlName){
        $scope.processing=true;
        UrlNames.getFromName(urlName).then(function(urlName){
            $rootScope.$broadcast('setCurrentUrlNameID',urlName.id);
            $scope.processing=false;
        })

    }

    $scope.setCurrentPage = function(currentPage){
        $scope.currentPage = currentPage;
    };

    $scope.keyLength = function(widgetName){
        return Object.keys($scope.similarWidgets[widgetName]).length;
    };
    $scope.loadSCSSFrom = function(widgetName){
        $scope.processing=true;
        var fromID = $("#loader_"+widgetName).val();
        $scope.wvariables={};
        $scope.wvariables2={};
        $scope.types={};
        ScssStore.loadWidgetSCSS(fromID, widgetName).then(function(){
            load();
        });
    };

    function load(){
        ScssStore.load2($scope).then(function(store){
            $scope.processing = true;
            store.loadPrimaryColors($scope.websiteID).then(function(c){

                $scope.primarycolorchoices=c;

                store.loadWidgets($scope,UrlNames,WidgetStyles, Preview).then(function(store){
                    $scope.processing = false;
                    $scope.store = store;
                    $scope.wvariables = $scope.store.getWvariables();
                    $scope.wvariables2 = $scope.store.getWvariables2();
                    $scope.similarWidgets = $scope.store.getSimilarWidgets()
                    $scope.types = store.types;
                    $scope.loaded = true;


                });
            });
        });
    }
    load();



};