'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.IDElayout', []);

//alias of seoPageData
//app.controller('layout',['$scope', '$rootScope', '$routeParams','$compile', 'Layouts', 'Options', 'Preview'
//        , function($scope, $rootScope, $routeParams,$compile, Layouts, Options, Preview){
controllers.layout = function($scope, $rootScope, $routeParams,$compile, Layouts, Options, Preview){
    $rootScope.$broadcast('setCurrent','websites');
    $rootScope.$broadcast('setCurrentW','design');
    $rootScope.$broadcast('setCurrentD','layout');
    $rootScope.$broadcast('setCurrentWebsiteID',$routeParams.websiteID);
    $rootScope.$broadcast('setCurrentUrlNameID',$routeParams.urlNameID);

    $scope.testAdmin = $rootScope.testAdmin;

    $scope.processing = false;
    $scope.websiteID = $routeParams.websiteID;
    $scope.urlNameID = $routeParams.urlNameID;


    function loadLayout(){
        Options.get($scope.websiteID).then(function(o){
            $scope.options = o;

            Layouts.get($scope.websiteID, $scope.urlNameID).then(function(l){
                $scope.layout = l;
                $scope.d = Document.new($scope.options.width, $scope.options.gutter, $scope.options.nbColumn, $scope);
                $scope.d.widgetMenu = false;

                Layouts.getRow0(l.id).then(function(r0){
                    console.debug("rows0");
                    console.debug(r0);
                    for(var i in r0){
                        var r = $scope.d.addRow();
                        r.nbColumn = r0[i].columns;
                        r.rowID = r0[i].id;
                        var rbis = r0[i];
                        r.containers = DoubleLinkedList.new();
                        (function(r,rbis){
                            Layouts.getContainers(rbis.id).then(function(c0){

                                for(var j in c0){
                                    var c00 = c0[j];
                                    var c = r.containers.insertEnd( Container.new(c0[j].columns, $scope.d, r));
                                    c.containerID = c0[j].id;
                                    $scope.draw();

                                    (function(c,c00){
                                        Layouts.getRows(c00.id).then(function(r1){
                                            if(r1.length>0){
                                                console.debug("HERE");
                                                for(var k in r1){
                                                    //this container c.id has some rows inside
                                                    var tmp = Row.new(r1[k].columns, $scope.d, c);
                                                    tmp.containers = DoubleLinkedList.new();
                                                    tmp.rowID = r1[k].id;
                                                    var r = c.rows.insertEnd( tmp );
                                                    (function(r1,k,r){

                                                        Layouts.getContainers(r1[k].id).then(function(c1){
                                                            for(var l in c1){
                                                                r.containers.insertEnd(Container.new(c1[l].columns,$scope.d, r));
                                                                r.containerID = c1[l].id;
                                                                $scope.draw();
                                                            }
                                                        });
                                                    })(r1,k,r);
                                                }
                                            }
                                        });
                                    })(c,c00);
                                }
                            });
                        })(r,rbis);
                    }
                    $scope.draw();
                });
            });
        });
    }
    loadLayout();


    $scope.editContainer = function($event){
        var elem = $($event.target);
        $("#context1").css({
            position: "absolute",
            //display: "block",
            left: $event.clientX + 'px',
            top: $event.clientY + 'px'
        }).show();

        $scope.activeContainer = getNode(elem.attr("id"));


        $(".su_container").each(function(){
            if($(this).css("border-width")!= "0px"){
                var width = parseInt( $(this).css("width"), 10) + 2;
                $(this).css("width", width+"px");
            }
            $(this).css("border","0px");
        });

        var width = parseInt( elem.css("width"), 10) - 2;
        elem.css("width", width+"px");
        //$(this).css("border-width", "1px");
        elem.css("border","1px solid #000");

    }



    $scope.update = function(){
        $scope.d.width = parseInt($scope.layout.width);
        $scope.d.gutter = parseInt($scope.layout.gutter);
        $scope.draw();
    };

    $scope.save = function(){
        var data = {};
        data.layout = $scope.layout;
        data.d = $scope.d.serialize();
        Preview.setRefreshStatus(true);
        Layouts.save($scope.layout.id, data).then(function(){
            loadLayout();
            Preview.setRefreshStatus(false);
        });


    };

    function getNode(nodeID){
        var data = nodeID.split("_");
        var currentObject = null;
        for(var i in data){
            var type = data[i].substr(0,1);
            var index = parseInt(data[i].substr( 1,1), 10);
            if(currentObject == null){
                //it is the first time
                currentObject = $scope.d.rows.getElementAt(index);
            }
            else{
                switch(type){
                    case "R" :
                        currentObject = currentObject.rows.getElementAt(index);
                        break;
                    case "C" :
                        currentObject = currentObject.containers.getElementAt(index);
                        break;
                }
            }
        }
        return currentObject;
    }


    $scope.activeContainer = "test2";

    $scope.draw = function(){
        $("#canvas").empty();

        $("#canvas").html( $compile( $scope.d.generateHTML(true))($scope));



        if($scope.activeContainer!=null){
            if($("#"+$scope.activeContainer.id).css("border-width") == "0px"){

                var width = parseInt( $("#"+$scope.activeContainer.id).css("width"), 10) - 2;
                $("#"+$scope.activeContainer.id).css("width", width+"px");
            }
            $("#"+$scope.activeContainer.id).css("border","1px solid #000");
        }
    };

    $scope.increaseWidth = function(){
        $scope.activeContainer.incrWidth();
        $scope.draw();
    };
    $scope.decreaseWidth = function(){
        $scope.activeContainer.decrWidth();
        $scope.draw();
    };
    $scope.remove = function(){
        $("#context1").hide();
        var nodeID = $scope.activeContainer.id;
        var data = nodeID.split("_");
        var currentObject = null;

        for(var i in data){
            var type = data[i].substr(0,1);
            var index = parseInt(data[i].substr( 1,1), 10);

            if(currentObject == null){
                //it is the first time
                currentObject = $scope.d.rows.getElementAt(index);
            }
            else{
                if(i == (data.length-1)){

                    switch(type){
                        case "R" :
                            throw "not here";
                            break;
                        case "C" :
                            currentObject.removeContainer(index);
                            break;
                    }
                }
            }
        }
        $scope.activeContainer = null;
        $scope.draw();
    };
    $scope.insertAfter = function(){
        var nodeID = $scope.activeContainer.id;
        var data = nodeID.split("_");
        var currentObject = null;

        for(var i in data){
            var type = data[i].substr(0,1);
            var index = parseInt(data[i].substr( 1,1), 10);

            if(currentObject == null){
                //it is the first time
                currentObject = $scope.d.rows.getElementAt(index);
            }
            else{

                switch(type){
                    case "R" :
                        currentObject = currentObject.rows.getElementAt(index);
                        break;
                    case "C" :
                        if(i == (data.length-1)){
                             currentObject.addContainerAfter($scope.activeContainer);
                        }
                        else{
                            currentObject = currentObject.containers.getElementAt(index);
                        }
                        break;
                }
            }

        }
        //console.debug($scope.d);
        $scope.draw();
    };
    $scope.insertBefore = function(){
        var nodeID = $scope.activeContainer.id;
        var data = nodeID.split("_");
        var currentObject = null;

        for(var i in data){
            var type = data[i].substr(0,1);
            var index = parseInt(data[i].substr( 1,1), 10);

            if(currentObject == null){
                //it is the first time
                currentObject = $scope.d.rows.getElementAt(index);
            }
            else{
                if(i == (data.length-1)){

                    switch(type){
                        case "R" :
                            throw "not here";
                            break;
                        case "C" :

                            currentObject.addContainerBefore($scope.activeContainer);
                            break;
                    }
                }
            }
        }
        $scope.draw();
    };

    $scope.addRow = function(){

        $scope.activeContainer.addRow();
        $scope.activeContainer = null;
        $scope.draw();
    }

    $scope.addRowDocument = function(){
        $scope.d.addRow();
        $scope.draw();
    };


};