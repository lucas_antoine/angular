'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.IDEglobal', []);



//alias of seoPageData
//app.controller('globalSCSS',['$scope', '$rootScope', '$routeParams', '$compile', '$q', 'WidgetStyles', 'WidgetThemeVariables', 'ScssStore', 'Color'
//        , function($scope, $rootScope, $routeParams, $compile, $q, WidgetStyles, WidgetThemeVariables, ScssStore, Color) {
controllers.globalSCSS = function($scope, $rootScope, $routeParams, $compile, $q, WidgetStyles, WidgetThemeVariables, ScssStore, Color) {
    $rootScope.$broadcast('setCurrent', 'websites');
    $rootScope.$broadcast('setCurrentW', 'design');
    $rootScope.$broadcast('setCurrentD', 'global');
    $rootScope.$broadcast('setCurrentWebsiteID', $routeParams.websiteID);

    $scope.testAdmin = $rootScope.testAdmin;

    $scope.processing = false;
    $scope.websiteID = $routeParams.websiteID;
    $scope.variablename = '';
    $scope.clipboard = "";
    $scope.loaded = false;

    $scope.newcolor=null;

    $scope.currentPage = "Colors";
    $scope.t = tinycolor("#081977");

    $scope.pages = {Colors: -1,SiteGeneral : -2, WidgetCommon: -5, Crumb: -3, Label: -4,  Typo: -8, sidenav: -6, table: -7 };


    $scope.fontfamilies = ['"Droid Sans", sans-serif','"Arvo", serif','"PT Sans", sans-serif','"Ubuntu", sans-serif','"Josefin Slab", serif','"Open Sans", sans-serif','"Open Sans Condensed", sans-serif','"Vollkorn", serif','"Abril Fatface", cursive','"Old Standard TT", serif','"Advent Pro", sans-serif'];


    $scope.primarycolorchoices = [];
    $scope.updateprimarycolors = function(){
        $scope.primarycolorchoices = [];
        for(var i in $scope.fondations['Colors']['Base']){
            $scope.primarycolorchoices.push($scope.fondations['Colors']['Base'][i]);
        }
    };
    var defer = $q.defer();


    ScssStore.load2($scope).then(function(store){
        console.debug("loaded");


        $scope.types = store.types;
        $scope.availableTypes = [];
        $scope.store=store;
        for(var type in $scope.types){
            $scope.availableTypes.push(type);
        }
        $scope.loaded = true;

        $scope.fondations = {};

        for(var i in store.fondations){
            $scope.fondations[i]=store.fondations[i];
        }
        $scope.updateprimarycolors();
        initColorChooser();
        if(!$scope.$$phase) {
            $scope.$digest();
        }
    });

    $scope.setCurrentPage = function(currentPage){
        $scope.currentPage = currentPage;
    };

    $scope.saveScheme = function(){
        Preview.setRefreshStatus(true);
        $scope.processing = true;
        var data = {};
        data.hex = $('#hex').val();
        data.schemeName = $scope.schemeName ? $scope.schemeName :  'analogous';

        Color.doSave($scope.websiteID, data).then(function(){
            $scope.processing = false;
            Preview.setRefreshStatus(false);
        });


    }

    function initColorValues(){
        var Hsv = $scope.t.toHsv();
        $("#hue-slider").slider({
            min: 0,
            max: 360,
            width : 600,
            step : 15,
            value : Hsv.h,
            slide: function(e, ui) {
                // ui.value has the number
                $scope.t = tinycolor("hsv "+ui.value+" "+$("#sat-slider").slider('value')+" "+$("#val-slider").slider('value'));
                updateColorValues();
            }
        });
        $("#sat-slider").slider({
            min: 0.01,
            step: 0.01,
            max: 1,
            slide: function(e, ui) {
                // ui.value has the number
                $scope.t = tinycolor("hsv "+ $("#hue-slider").slider('value')+" "+ui.value+" "+$("#val-slider").slider('value'));
                updateColorValues();
            }
        });
        $("#val-slider").slider({
            min: 0.01,
            step: 0.01,
            max: 1,
            slide: function(e, ui) {
                // ui.value has the number
                $scope.t = tinycolor("hsv "+ $("#hue-slider").slider('value')+" "+$("#sat-slider").slider('value')+" "+ui.value);
                updateColorValues();
            }
        });
        $('#hex').val($scope.t.toHexString());
        $("#hex-box").css("background-color", $scope.t.toHexString());
    }

    $scope.$on('setSchemeColor', function(event,color) {
        $scope.setMainColor(color);
    });
    $scope.setMainColor = function(color){
        $('#hex').val(color);
        $scope.t = tinycolor(color);
        updateColorValues();
    };

    function updateColorValues(){
        var Hsv = $scope.t.toHsv();
        var color = $scope.t.toHexString();


        $("#hue-slider").slider({value:Hsv.h});
        $("#sat-slider").slider({value:Hsv.s});
        $("#val-slider").slider({value:Hsv.v});

        $('#hex').val($scope.t.toHexString());
        $("#hex-box").css("background-color", $scope.t.toHexString());

        $("#lightened").css("background-color", tinycolor.lighten(color,10).toHexString());
        $("#darkened").css("background-color", tinycolor.darken(color,10).toHexString());
        $("#desaturated").css("background-color", tinycolor.desaturate(color,10).toHexString());
        $("#saturated").css("background-color", tinycolor.saturate(color,10).toHexString());
        $("#greyscaled").css("background-color", tinycolor.greyscale(color).toHexString() );
        $scope.setScheme($scope.schemeName);

    }

    $scope.setScheme = function(name){
        $scope.schemeName = name;
        var color = $scope.t.toHexString();
        var colors = [];
        switch(name){
            case "analogous" :
                colors = tinycolor.analogous(color)
                break;
            case "monochromatic" :
                colors = tinycolor.monochromatic(color);
                break;
            case "split" :
                colors = tinycolor.splitcomplement(color);
                break;
            case "triad" :
                colors = tinycolor.triad(color);
                break;
            case "tetrad" :
                colors = tinycolor.tetrad(color);
                break;
            case "square" :
                colors = tinycolor.square(color);
                break;
            case "complement" :
                colors = tinycolor.complement(color);
                break;
        }

        generateColors(colors);
    };

    function generateColors(colors){
        $('#colors').html('');
        for (var i in colors) {
            var c = colors[i];
            var newDiv = '<span style="cursor : pointer;background-color: #' + c.toHex() + ';" class="color"  ng-click="cp(\'' + c.toHexString() + '\');"></span><span class="text">'+c.toHexString()+'</span>';
            $('#colors').append($compile(newDiv)($scope));
        }
    }

    function initColorChooser(){
        Color.get($scope.websiteID).then(function(c){
            if(c!=[]){
                $scope.t = tinycolor(c.hex);
                $scope.schemeName = c.schemeName;
            }
            else{
                $scope.t = tinycolor("#181977");
                $scope.schemeName = "triad";
            }
            initColorValues();
            updateColorValues();

            $('#set-hex').click(function() {
                $scope.t = tinycolor($("#hex").val());
                updateColorValues();
            });
            $('#paste-hex').click(function() {
                if($scope.clipboard){
                    $('#hex').val($scope.clipboard);
                }
                $scope.t = tinycolor($("#hex").val());
                updateColorValues();
            });

        });

    }

    $("#lightened").click(function(){$scope.setMainColor($(this).css("background-color"));});
    $("#darkened").click(function(){$scope.setMainColor($(this).css("background-color"));});
    $("#desaturated").click(function(){$scope.setMainColor($(this).css("background-color"));});
    $("#saturated").click(function(){$scope.setMainColor($(this).css("background-color"));});
    $("#greyscaled").click(function(){$scope.setMainColor($(this).css("background-color"));});

    $scope.cp = function(color) {
        $scope.clipboard = color;
    };
    $scope.paste = function(dest) {
        $scope[dest] = $scope.clipboard;
    };
};