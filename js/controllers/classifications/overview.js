/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.CMatchOverview', []);

//app.controller('matchOverview', ['$scope','$rootScope', "Types", 'Sources', 'Classifications','Overview', 
//function($scope, $rootScope, Types, Sources, Classifications, Overview){
controllers.matchOverview = function($scope, $rootScope, Types, Sources, Classifications, Overview) {
    $rootScope.$broadcast('setCurrent', 'classifications');
    $rootScope.$broadcast('setCurrentC', 'resolution');
    $scope.testAdmin = $rootScope.testAdmin;

    $scope.processing = false;
    $scope.adminEnable = false;
    $scope.editEnable = false;
    $scope.option = 1;
    
    $scope.$on('cleanMatchingTree', function(event) {
        Overview.cleanMatchingTree();
    });

    Overview.getTypes().then(function(t) {
        $scope.types = t;
        Overview.getMaster().success(function(m) {
            $scope.main = m;
            console.debug(m);
            Overview.get().then(function(o) {
                console.debug(o);
                $scope.overview = o;
            });
        });
    });
    $scope.inArray = function(value, array)
    {
        
        for(var i in array){
            if(array[i] == value){
                return true;
            }
        }
        return false;

    };


};




