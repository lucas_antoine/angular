'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.CMatchInterface', []);

//app.controller('matchInterface', ['$scope', '$rootScope', '$routeParams', '$timeout', "Types", 'Sources', 'Classifications', 'Overview',
//function($scope, $rootScope, $routeParams, $timeout, Types, Sources, Classifications, Overview) {
controllers.matchInterface = function($scope, $rootScope, $routeParams, $timeout, Types, Sources, Classifications, Overview) {
    $rootScope.$broadcast('setCurrent', 'classifications');
    $rootScope.$broadcast('setCurrentC', 'resolution');

    $scope.adminEnable = false;
    $scope.editEnable = false;
    $scope.option = 1;

    $scope.sourceID = $routeParams.sourceID;
    $scope.againstID = $routeParams.againstID;


    Overview.getTree($scope.sourceID, $scope.againstID).then(function(t1) {
        $scope.tree1 = t1;
        console.debug(t1);
        Overview.getTree($scope.againstID, $scope.sourceID).then(function(t2) {
            $scope.tree2 = t2;
            console.debug(t2);
            if ($scope.tree1.data.length > 0 && $scope.tree2.data.length > 0) {
                //draw();
            }
        });
    });

    $scope.leftActiveID = 0;
    $scope.rightActiveID = 0;
    $scope.connections = [];

    var hoverPaintStyle = {strokeStyle: "#7ec3d9"};



    var styles = {
        1: {//not enough jobs
            lineWidth: 0.7,
            strokeStyle: "blue",
            outlineWidth: 0,
            outlineColor: "#666"
        },
        2: {//equal
            lineWidth: 0.7,
            strokeStyle: "yellow",
            outlineWidth: 0,
            outlineColor: "#666"
        },
        3: {//set
            lineWidth: 0.7,
            strokeStyle: "green",
            outlineWidth: 0,
            outlineColor: "#666"
        },
        4: {//unset
            lineWidth: 0.7,
            strokeStyle: "red",
            outlineWidth: 0,
            outlineColor: "#666"
        },
        5: {//low accuracy Ratio
            lineWidth: 0.7,
            strokeStyle: "darkcyan",
            outlineWidth: 0,
            outlineColor: "#666"
        }
    };




    var common = {
        connector: ["Straight", {stub: 40}],
        endpoint: "Blank",
        anchors: ["RightMiddle", 'LeftMiddle'],
        hoverPaintStyle: hoverPaintStyle,
        endpointStyle: {fillStyle: "#a7b04b"},
        detachable: true
    };

    function removeAllConnections() {
        jsPlumb.ready(function() {
            for (var i in $scope.connections) {
                for (var j in $scope.connections[i]) {
                    try {
                        jsPlumb.detach($scope.connections[i][j]);
                    }
                    catch (err) {
                        console.debug(err);
                    }
                }
            }
            $scope.connections = [];
        });
    }

    function drawLeftID(subjectID) {
        removeAllConnections();
        console.debug("DrawLeft()");


        $scope.connections = [];
        for (var i in $scope.tree1.raw) {
            if ($scope.tree1.raw[i].id == subjectID) {
                var leftID = "C" + $scope.tree1.raw[i].id;
                console.debug("Found leftID " + leftID);
                $scope.connections[leftID] = [];
                for (var ii in $scope.tree1.raw[i].complements) {
                    var rightID = "C" + $scope.tree1.raw[i].complements[ii].complementID;
                    console.debug("Found rightID " + rightID);
                    var style = styles[$scope.tree1.raw[i].complements[ii].matchStatusID];

                    $scope.connections[leftID][rightID] = jsPlumb.connect({
                        source: leftID,
                        target: rightID,
                        paintStyle: style,
                        events: {
                            "click": function(connection, originalEvent) {
                                $scope.unlink(connection);
                            }
                        }
                    }, common);
                }
            }
        }
    }

    function drawRightID(complementID) {
        removeAllConnections();
        $scope.connections = [];
        jsPlumb.ready(function() {

            for (var i in $scope.tree2.raw) {
                if ($scope.tree2.raw[i].id == complementID) {
                    var rightID = "C" + $scope.tree2.raw[i].id;

                    for (var ii in $scope.tree2.raw[i].complements) {
                        var leftID = "C" + $scope.tree2.raw[i].complements[ii].complementID;
                        $scope.connections[leftID] = [];

                        var style = styles[$scope.tree2.raw[i].complements[ii].matchStatusID];

                        console.debug(leftID);
                        console.debug(rightID);

                        $scope.connections[leftID][rightID] = jsPlumb.connect({
                            source: rightID,
                            target: leftID,
                            paintStyle: style,
                            events: {
                                "click": function(connection, originalEvent) {
                                    $scope.unlink(connection);
                                }
                            }
                        }, common);
                    }
                }
            }
        });
    }

    function draw() {
        removeAllConnections();
        console.debug("Draw()");
        jsPlumb.ready(function() {

            $timeout(function() {
                for (var i in $scope.tree1.raw) {
                    var leftID = "C" + $scope.tree1.raw[i].id;
                    $scope.connections[leftID] = [];
                    for (var ii in $scope.tree1.raw[i].complements) {
                        var rightID = "C" + $scope.tree1.raw[i].complements[ii].complementID;
                        var style = styles[$scope.tree1.raw[i].complements[ii].matchStatusID];

                        $scope.connections[leftID][rightID] = jsPlumb.connect({
                            source: leftID,
                            target: rightID,
                            paintStyle: style,
                            events: {
                                "click": function(connection, originalEvent) {
                                    $scope.unlink(connection);
                                }
                            }
                        }, common);


                    }
                }
            });

        });
    }

    $scope.clear = function() {
        $scope.rightActiveID = 0;
        $scope.leftActiveID = 0;
        removeAllConnections();
    };

    $scope.setLeftActive = function(tr) {
        console.debug(tr);
        if ($scope.rightActiveID != 0) {
            if (createMatch(tr.id, $scope.rightActiveID) == false) {
                $scope.rightActiveID = 0;
                $scope.leftActiveID = tr.id;
                update();
            }
        }
        else {
            $scope.leftActiveID = tr.id;
            update();
        }

    };

    $scope.setRightActive = function(tr) {
        console.debug(tr);
        if ($scope.leftActiveID != 0) {
            if (createMatch($scope.leftActiveID, tr.id) == false) {
                $scope.leftActiveID = 0;
                $scope.rightActiveID = tr.id;
                update();
            }
        }
        else {
            $scope.rightActiveID = tr.id;
            update();
        }
    };

    function update() {
        Overview.getTree($scope.sourceID, $scope.againstID).then(function(t) {
            $scope.tree1 = t;
            Overview.getTree($scope.againstID, $scope.sourceID).then(function(t) {
                $scope.tree2 = t;
                if ($scope.leftActiveID != 0) {
                    drawLeftID($scope.leftActiveID);
                }
                else if ($scope.rightActiveID != 0) {
                    drawRightID($scope.rightActiveID);
                }
                else {
                    draw();
                }
            });
        });

    }

    $scope.unlink = function(connection) {
        var sourceID = connection.sourceId;
        var targetID = connection.targetId;


        Overview.deleteMatch(sourceID, targetID).then(function() {
            jsPlumb.ready(function() {
                jsPlumb.detach(connection);
            });
            update();

        });

    };

    $("#left").scroll(function() {
        jsPlumb.ready(function() {
            jsPlumb.repaintEverything();
        });
    });

    $("#right").scroll(function() {
        jsPlumb.ready(function() {
            jsPlumb.repaintEverything();
        });
    });

    function createMatch(subjectID, complementID) {
        if (subjectID == 0 || complementID == 0) {
            return false;
        }
        var leftID = "C" + subjectID;
        var rightID = "C" + complementID;
        if ($scope.connections[leftID] != undefined && $scope.connections[rightID] != undefined) {
            return false;
        }

        else {
            Overview.createMatch(subjectID, complementID).then(function() {
                update();
            });

        }
    }








};