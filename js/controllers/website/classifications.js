/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//var app = angular.module('myApp.Wclassification', []);

//app.controller('adminWebsiteClassifications', ['$scope', '$rootScope', '$routeParams', 'Types', 'Sources', 'Websites', 'Sphinx',
//    function($scope, $rootScope, $routeParams, Types, Sources, Websites, Sphinx) {
controllers.adminWebsiteClassifications = function($scope, $rootScope, $routeParams, Types, Sources, Websites, Sphinx) {

    $rootScope.$broadcast('setCurrent', 'websites');
    $rootScope.$broadcast('setCurrentW', 'classification');
    $rootScope.$broadcast('setCurrentWebsiteID', $routeParams.websiteID);

    $scope.testAdmin = $rootScope.testAdmin;

    $scope.websites = [];
    $scope.websiteID = $routeParams.websiteID;


    Websites.get($scope.websiteID).then(function(w) {
        $scope.w = w;
        getClassificationList(w.id);
        $scope.wid = w.id;
        //$rootScope.$broadcast('setCurrentWebsiteID',w.id);
        //$rootScope.$broadcast('setCurrentW',['general']);
        Websites.getTypesSources($scope.websiteID).then(function(t) {
            $scope.types = t;
        });
    }, function(reason) {
        alert(reason);
    });

    $scope.changeOrigin = function(type) {

        Websites.changeSource(type, $scope.websiteID).then(function(r) {
            Sphinx.generateIndex($scope.websiteID).then(function() {
                Sphinx.rotate($scope.websiteID).then(function() {

                });
            });
        });
    };

    $scope.nt = 0;
    $scope.ns = 0;
    $scope.wid = 0;
    $scope.ncid = 0;





    $scope.w = {};
    $scope.lvl0 = {};
    $scope.lvl1 = {};
    $scope.lvl2 = {};
    $scope.lvl3 = {};
    $scope.lvl4 = {};
    $scope.lvl5 = {};
    $scope.lvl0List = {};
    $scope.lvl1List = {};
    $scope.lvl2List = {};
    $scope.lvl3List = {};
    $scope.lvl4List = {};
    $scope.lvl5List = {};

    $scope.linkClassification = function() {

        Websites.link($scope.wid, $scope.ncid).then(function(ret) {
            if (ret.error) {
                alert(ret.reason);
            }
            else {
                getClassificationList($scope.w.id);
                $scope.toggleAddClassification();

                Sphinx.generateIndex($scope.wid).then(function() {
                    Sphinx.rotate($scope.wid);
                });
            }

        }, function(reason) {
            console.debug(reason);
        });
    };

    $scope.unlinkClassification = function(cid) {

        Websites.unlink($scope.wid, cid).then(function() {
            getClassificationList($scope.w.id);
            
            Sphinx.generateIndex($scope.wid).then(function() {
                Sphinx.rotate($scope.wid);
            });
            
            
        }, function(reason) {
            console.debug(reason);
        });
    };

    $scope.addClassification = false;

    $scope.toggleAddClassification = function() {
        $scope.addClassification = !$scope.addClassification;
        if ($scope.addClassification) {
            $scope.ncid = 0;
            $scope.nt = 0;
            $scope.ns = null;
        }
    };

    $scope.setType = function(type) {


        for (var i in $scope.types) {

            if ($scope.types[i].id == type) {
                $scope.nt = type;
                $scope.ns = $scope.types[i].classificationOriginID;
                console.debug($scope.nt);
                console.debug($scope.ns);
            }
        }

    };


    function getClassificationList(websiteID) {
        Websites.listClassifications(websiteID).then(function(w) {
            console.debug(w);
            $scope.listClassifications = w;
        }, function(reason) {
            alert(reason);
        });
    }



};
