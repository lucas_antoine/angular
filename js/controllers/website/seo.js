'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.Wseo', []);

//alias of seoPageData
//app.controller('seo', ['$scope', '$rootScope', '$routeParams','SeoPages', 'SeoPageData',function($scope, $rootScope, $routeParams,SeoPages, SeoPageData){
controllers.seo = function($scope, $rootScope, $routeParams,SeoPages, SeoPageData){

    $rootScope.$broadcast('setCurrent','websites');
    $rootScope.$broadcast('setCurrentW','seo');
    $rootScope.$broadcast('setCurrentS','data');
    $rootScope.$broadcast('setCurrentWebsiteID',$routeParams.websiteID);
    $scope.testAdmin = $rootScope.testAdmin;

    SeoPageData.getAvailableVariables().then(function(d){
        $scope.availableVariables= d;
    });

    function load(){
        $scope.processing=true;
        SeoPageData.getAll($routeParams.websiteID).then(function(d){
            $scope.seoPageData = d;
            $scope.processing=false;
        });
    }
    load();

    $scope.save = function(seo){
        $scope.processing=true;
        if(seo.title=="" && seo.description=="" && seo.customText==""){
            if(seo.id!=null){

                SeoPageData.doDelete(seo).then(function(){
                   load();
                });
            }
            else{
                $scope.processing=false;
            }

        }
        else{
            seo.websiteID = $routeParams.websiteID;
            seo.seoPageID = seo.urlID;
            if(seo.id!=null){

                SeoPageData.doSave(seo).then(function(){
                    load();
                });
            }
            else{
                SeoPageData.doCreate(seo).then(function(){
                    load();
                });
            }
        }
    }

};

//app.controller('seoPages', ['$scope', '$rootScope','SeoPages', 'SeoPageData',function($scope, $rootScope,SeoPages, SeoPageData){
controllers.seoPages = function($scope, $rootScope,SeoPages, SeoPageData){

    $rootScope.$broadcast('setCurrent','seo');
    $scope.testAdmin = $rootScope.testAdmin;

    SeoPageData.getAvailableVariables().then(function(d){
        $scope.availableVariables= d;
    });

    function load(){
        $scope.processing = true;
        SeoPages.getSpecialFunctions().then(function(m){
            $scope.specialFunctions = m;
            SeoPages.getAll().then(function(s){
                $scope.seoPages = s;

                $scope.processing = false;
            });
        });


    }
    load();

    $scope.create = function(seo){
        $scope.processing=true;
        SeoPages.doCreate(seo).then(function(){
            load();
            $scope.newseo = {};
        });
    };

    $scope.delete = function(seo){
        $scope.processing=true;
        SeoPages.doDelete(seo).then(function(){
            load();
        });
    };

    $scope.save = function(seo){
        $scope.processing=true;
        SeoPages.doSave(seo).then(function(){
            $scope.processing=false;
        });
    };
};


