
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.Wcontrollers', []);

//app.controller('adminWebsite', ['$scope','$rootScope',  '$routeParams',  'Websites', 'Sphinx',
//    function($scope,$rootScope,  $routeParams, Websites, Sphinx){

controllers.adminWebsite = function($scope, $rootScope, $routeParams, Websites, Sphinx, Origins) {

    $rootScope.$broadcast('setCurrent', 'websites');
    $rootScope.$broadcast('setCurrentW', 'general');
    $rootScope.$broadcast('setCurrentWebsiteID', $routeParams.websiteID);

    $scope.testAdmin = $rootScope.testAdmin;

    $scope.processing = false;
    $scope.websites = [];
    $scope.websiteID = $routeParams.websiteID;

    function updateIndexer(){
        console.debug("Updating index");
        Sphinx.generateIndex($scope.websiteID).then(function() {
            Sphinx.rotate($scope.websiteID).then(function(){
                console.debug("index generated")
            });
        });
    }
    
    $scope.updateIndexer = function(){
        updateIndexer();
    }

    Origins.getWebsiteOrigins($scope.websiteID).success(function(o) {
        $scope.availableOrigins = o;
    });

    if ($scope.websiteID == 0) {
        $scope.isNew = true;
        $scope.wForm = true;
        $("#websiteSelect").val(0);
        $scope.w = {
            websiteName: "",
            url: "",
            IP: "",
            facebookPageID: "",
            googleAnalyticID: "",
            googlePlusID: "",
            bingKey: "",
            googleSiteVerificationID: "",
            maxJobDays: null,
            linkedInID: ""
        };

    }
    else {
        Websites.get($scope.websiteID).then(function(w) {
            $scope.wForm = true;
            $scope.w = w;
            $scope.wid = w.id;
            $scope.isNew = false;
            //$rootScope.$broadcast('setCurrentWebsiteID',w.id);
            //$rootScope.$broadcast('setCurrentW',['general']);
        }, function(reason) {
            alert(reason);
        });
    }


    //hide the website form
    $scope.wForm = true;




    $scope.isNew = true;
    $scope.w = {};

    $scope.updateOrigin = function(o){
        Websites.setJobOrigin($scope.websiteID, o);
    };

    $scope.save = function(s) {
        Websites.doSave(s).then(function() {

            updateIndexer();
        }, function(reason) {
            console.debug(reason);
        });

    };

    $scope.remove = function(s) {
        $scope.processing = true;
        var promise = Websites.doDelete(s);
        promise.then(function() {
            
            Sphinx.removeIndex($scope.websiteID);
            $scope.wForm = false;
            $scope.isNew = true;
            $scope.processing = false;
        }, function(reason) {
            console.debug(reason);
        });
    };

    $scope.create = function(source) {
        $scope.processing = true;
        var promise = Websites.doCreate(source);
        promise.then(function(result) {
            var promise2 = Websites.get(result);
            promise2.then(function(w) {
                $scope.wForm = true;
                $scope.w = w;
                //getClassificationList(w.id);
                $scope.wid = w.id;
                $scope.websiteID = w.id;

                $rootScope.$broadcast('newWebsiteID', $scope.wid);

                updateIndexer();
            }, function(reason) {
                alert(reason);
            });
            $scope.isNew = false;
            $scope.processing = false;
        }, function(reason) {
            console.debug(reason);
        });

    };

};




