'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//var app = angular.module('myApp.Wdev', []);

//app.controller('dev', ['$scope', '$rootScope', '$routeParams','Types', 'Websites','Options', function($scope, $rootScope, $routeParams,Types, Websites,Options){

controllers.dev = function($scope, $rootScope, $routeParams, Types, Websites, Options) {

    $rootScope.$broadcast('setCurrent', 'websites');
    $rootScope.$broadcast('setCurrentW', 'dev');
    $rootScope.$broadcast('setCurrentWebsiteID', $routeParams.websiteID);
    $scope.testAdmin = $rootScope.testAdmin;

    $scope.processing = false;
    $scope.websiteID = $routeParams.websiteID;
    $scope.possibleWidths = [750, 960, 974, 1024];

    Options.get($scope.websiteID).then(function(o) {
        $scope.option = o;
        Websites.get($scope.websiteID).then(function(w) {
            $scope.website = w;
        });
    });


    $scope.save = function(o) {
        $scope.processing = true;
        Options.doSave(o).then(function(a) {
            Options.get($scope.websiteID).then(function(o) {
                $scope.option = o;
                $scope.processing = false;
            });
        });
    }

};

