'use strict';
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */

var app = angular.module('ide', ['ui.bootstrap', /*'ui.sortable'*/, 'ui.select2', 'ngRoute',  'googlechart']);

var directivePath = "http://jscommon:8181/directives/";
var restPath = "http://rest:8181/index.php/";

app.controller(controllers);
app.directive(directives);
app.service(services);
app.factory(factories);



app.run(['$rootScope', '$templateCache', function($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function() {
            $templateCache.removeAll();
        });
    }]);




app.config(['$sceDelegateProvider', '$routeProvider', '$httpProvider', '$compileProvider', '$provide', function($sceDelegateProvider, $routeProvider, $httpProvider, $compileProvider, $provide) {

        $sceDelegateProvider.resourceUrlWhitelist(['self', 'http://jscommon:8181/**', 'http://rest:8181/index.php/**']);
        
        
        $routeProvider.
                when('/', {templateUrl: 'partials/welcome.html'}).
                when('/managerTypes', {templateUrl: 'partials/classifications/manageTypes.html', controller: 'adminType'}).
                when('/managerClassificationOrigin', {templateUrl: 'partials/classifications/manageClassificationOrigins.html', controller: 'adminClassificationOrigins'}).
                when('/managerClassification/:typeID/:originID', {templateUrl: 'partials/classifications/manageClassifications.html', controller: 'adminClassification'}).
                when('/managerClassification', {templateUrl: 'partials/classifications/manageClassifications.html', controller: 'adminClassification'}).
                when('/managerMatching', {templateUrl: 'partials/classifications/matchOverview.html', controller: 'matchOverview'}).
                when('/matchInterface/:sourceID/:againstID', {templateUrl: 'partials/classifications/matchInterface.html', controller: 'matchInterface'}).
                when('/managerSourceDefaultWebsite', {templateUrl: 'partials/classifications/managerSourceDefaultWebsite.html', controller: 'adminDefaultSource'}).
                when('/managerWebsite/:websiteID', {templateUrl: 'partials/website/manageWebsites.html', controller: 'adminWebsite'}).
                when('/managerWebsiteClassifications/:websiteID', {templateUrl: 'partials/website/manageWebsiteClassifications.html', controller: 'adminWebsiteClassifications'}).
                when('/selectTest', {templateUrl: 'partials/selectTest.html', controller: 'SelectTestCtrl'}).
                when('/websites/:websiteId', {templateUrl: 'partials/website/details.html', controller: 'WebsiteDetailsCtrl'}).
                //Url
                when('/managerUrl/:websiteID', {templateUrl: 'partials/website/url/manageUrls.html', controller: 'adminUrls'}).
                when('/managerHtAccess/:websiteID', {templateUrl: 'partials/website/manageHtAccess.html', controller: 'htaccess'}).
                when('/manageUrl/:urlID', {templateUrl: 'partials/website/url/manageUrl.html', controller: 'urlParams'}).
                //url Global
                when('/managerObjects', {templateUrl: 'partials/url/manageObjects.html', controller: 'attributes'}).
                when('/managerLiterals', {templateUrl: 'partials/url/manageLiterals.html', controller: 'literals'}).
                when('/managerInstanciators', {templateUrl: 'partials/url/manageInstanciators.html', controller: 'instanciators'}).
                when('/managerUrlNames', {templateUrl: 'partials/url/manageUrlNames.html', controller: 'urlNames'}).
                //SEO Global
                when('/managerSEOPages', {templateUrl: 'partials/seo/manageSEOPages.html', controller: 'seoPages'}).
                //website
                when('/managerSEO/:websiteID', {templateUrl: 'partials/website/manageSEO.html', controller: 'seo'}).
                when('/managerDEV/:websiteID', {templateUrl: 'partials/website/manageDEV.html', controller: 'dev'}).
                //Layout
                when('/layoutDefinition', {templateUrl: 'partials/ide/layoutDefinition.html', controller: 'layout'}).
                when('/managerLayout/:websiteID/:urlNameID', {templateUrl: 'partials/ide/managerLayout.html', controller: 'layout'}).
                when('/managerGlobalSCSS/:websiteID', {templateUrl: 'partials/ide/global.html', controller: 'globalSCSS'}).
                when('/managerWidgetPosition/:websiteID/:urlNameID', {templateUrl: 'partials/ide/widgetPosition.html', controller: 'widgetPosition'}).
                when('/managerWidgetSCSS/:websiteID', {templateUrl: 'partials/ide/widgetSCSS.html', controller: 'widgetSCSS'}).
                //Spiders
                when('/managerSpiderStatus/:spiderID', {templateUrl: 'partials/spider/status.html', controller: 'spiderStatus'}).
                when('/managerImportClassifications/:spiderID', {templateUrl: 'partials/spider/import.html', controller: 'spiderImport'}).
                when('/managerSearchURLBuilder/:spiderID', {templateUrl: 'partials/spider/urlBuilder.html', controller: 'searchUrlBuilder'}).
                when('/managerRegexDefinition/:spiderID', {templateUrl: 'partials/spider/regexDefinition.html', controller: 'regexDefinition'}).
                when('/managerRegexes/:spiderID/:regexID', {templateUrl: 'partials/spider/regexes.html', controller: 'regexes'}).
                when('/testRegex/:spiderID/:spiderConfigID', {templateUrl: 'partials/spider/testRegex.html', controller: 'testRegex'}).
                when('/managerNewSpider', {templateUrl: 'partials/spider/create.html', controller: 'spiderCreate'}).
                when('/managerSpider/:spiderID', {templateUrl: 'partials/spider/edit.html', controller: 'spiderEdit'}).
                when('/spider/results/:spiderID', {templateUrl: 'partials/spider/results.html', controller: 'spiderResults'}).
                //Reports
                when('/reportTestChart', {templateUrl: 'partials/reports/testChart.html', controller: 'testGoogleChart'}).
                when('/reportSpiders', {templateUrl: 'partials/reports/spiders.html', controller: 'reportSpiders'}).
                when('/reportSpiderLog', {templateUrl: 'partials/reports/spiderLog.html', controller: 'reportSpiderLog'}).
                when('/reportSpiderLog/:spiderID', {templateUrl: 'partials/reports/spiderLog.html', controller: 'reportSpiderLog'}).
                when('/reportSphinx', {templateUrl: 'partials/reports/sphinx.html', controller: 'reportSphinx'}).
                when('/reportLogs', {templateUrl: 'partials/reports/logs.html', controller: 'reportLogs'}).
                when('/resolutionErrors', {templateUrl: 'partials/reports/resolutionErrors.html', controller: 'resolutionErrors'}).
                when('/multipleMatchsResolutionErrors/:spiderID/:typeID/:originID', {templateUrl: 'partials/reports/multipleMatchsResolutionErrors.html', controller: 'multipleMatchsResolutionErrors'}).
                otherwise({redirectTo: '/'});




        $provide.factory('loadingInterceptor', function($q) {
            var loadingInterceptor = {
                nb: 0
            };
            
            
            
            

            loadingInterceptor.request = function(config) {
                
                
                // same as above
                
                config.headers['X-Angular'] = "ide";
                
                if(config.noInterceptor && config.noInterceptor === true){ 
                    return config || $q.when(config);
                }
                
                
                loadingInterceptor.nb++;
                $("#loading").show();
                return config || $q.when(config);
            };

            loadingInterceptor.responseError = function(response) {
                
                loadingInterceptor.nb--;
                if (loadingInterceptor.nb == 0) {
                    $("#loading").hide();
                }
                // do something on error
                if (response.data.type == "myException") {
                    
                    $("#loading").hide();
                    //show Exception Window.
                    var ex = response.data;
                    var html = '<h3>Exception : ' + ex.message + '</h3>Line : ' + ex.line + '<br /><br />File: ' + ex.file + '<br /><br />\n\
    <div style="float:left;overflow : auto;height : 450px;width : 100%%;"><ul>';
                    for (var i in ex.trace) {
                        html += "<li>File : " + ex.trace[i].file + "<br />Line : " + ex.trace[i].line + "<BR/>Function : " + ex.trace[i].function + "</li>";
                    }
                    html += "</ul>";
                    $("#Exception").html(html).show();

                }
                
                
                return $q.reject(response);
            };
            
            loadingInterceptor.response = function(response) {
                if(response.config.noInterceptor && response.config.noInterceptor === true){    
                    return response || $q.when(response);                
                }
                
                
                // same as above
                loadingInterceptor.nb--;
                if (loadingInterceptor.nb == 0) {
                    $("#loading").hide();
                }
                
                return response || $q.when(response);
            };

            return loadingInterceptor;

        });


        $httpProvider.interceptors.push('loadingInterceptor');
        /*
         return function(promise) {
         numLoadings++;
         console.debug("showing");
         $("#loading").show();
         var hide = function() {
         numLoadings--;
         if (numLoadings==0){
         $("#loading").hide();
         console.debug("hiding");
         }
         };
         return promise.then(function(response) {
         hide();
         return response;
         }, function(response) {
         hide();
         if (response.data.type == "myException") {
         //show Exception Window.
         var ex = response.data;
         var html = '<h3>Exception : ' + ex.message + '</h3>Line : ' + ex.line + '<br /><br />File: ' + ex.file + '<br /><br />\n\
         <div style="float:left;overflow : auto;height : 450px;width : 100%%;"><ul>';
         for (var i in ex.trace) {
         html += "<li>File : " + ex.trace[i].file + "<br />Line : " + ex.trace[i].line + "<BR/>Function : " + ex.trace[i].function + "</li>";
         }
         html += "</ul>";
         $("#Exception").html(html).show();
         
         }
         
         
         
         
         return response;
         });
         };
         });*/

        var elementsList = $();

        var showMessage = function(content, cl, time) {
            $('<div/>')
                    .addClass('message')
                    .addClass(cl)
                    .hide()
                    .fadeIn('fast')
                    .delay(time)
                    .fadeOut('fast', function() {
                        $(this).remove();
                    })
                    .appendTo(elementsList)
                    .text(content);
        };
        /*
         $httpProvider.responseInterceptors.push(function($timeout, $q) {
         return function(promise) {
         return promise.then(function(successResponse) {
         if (successResponse.config.method.toUpperCase() != 'GET')
         showMessage('Success', 'successMessage', 5000);
         return successResponse;
         
         }, function(errorResponse) {
         switch (errorResponse.status) {
         case 401:
         showMessage('Wrong usename or password', 'errorMessage', 20000);
         break;
         case 403:
         showMessage('You don\'t have the right to do this', 'errorMessage', 20000);
         break;
         case 500:
         showMessage('Server internal error: ' + errorResponse.data, 'errorMessage', 20000);
         break;
         default:
         showMessage('Error ' + errorResponse.status + ': ' + errorResponse.data, 'errorMessage', 20000);
         }
         return $q.reject(errorResponse);
         });
         };
         });
         */
        $compileProvider.directive('appMessages', function() {
            var directiveDefinitionObject = {
                link: function(scope, element, attrs) {
                    elementsList.push($(element));
                }
            };
            return directiveDefinitionObject;
        });
    }]);
